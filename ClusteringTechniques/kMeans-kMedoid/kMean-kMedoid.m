%%%%%%%%HW2%%%%%%%%%
% clear
% load IrisDataAnnotated
% figure(6)
% hold on;
% plot(X(3,1:50),X(4,1:50),'* g',X(3,51:100),X(4,51:100),'* r',...
%     X(3,101:150),X(4,101:150), '* b', 'LineWidth', 60)
% legend('Setosa', 'Virginica', 'Versacolor', 'Location','northwest');
% xlabel('Petal Length', 'FontSize', 20); 
% ylabel('Petal Width', 'FontSize', 20);
% title('Iris Annotated plot', 'FontSize', 20);
% n=150;
% rng(1);
% k = randperm(n);
% k1 = k(1:round(n/3));
% k2 = k(round(n/3)+1:round(2*n/3));
% k3 = k(round(2*n/3)+1:n);
% 
% D1 = X(:,k1);
% D2 = X(:,k2);
% D3 = X(:,k3);
% %Assigning Each cluster index into different group 
% CS1 = k1;
% CS2 = k2;
% CS3 = k3;
% 
% [x1, l1] = size(k1);
% [x2, l2] = size(k2);
% [x3, l3] = size(k3);
% coh=[];
% oldCoh = 10000; %Intializing coherence with random values
% newCoh = 5000;
% 
% for j = 1 : 100
% 
%     fprintf('Value of Coherence %d  %d\n',oldCoh , newCoh)
%     if((oldCoh - newCoh) < 0.01)
%         fprintf('Value of Tau %d\n',oldCoh - newCoh)
%         break;
%     end
%     CS1 = [];
%     CS2 = [];
%     CS3 = [];
%     [~, d11] = size(D1);
%     [~, d22] = size(D2);
%     [~, d33] = size(D3);
%     %Calculating the centroids
%     Cl1 = (1/d11)*sum(D1,2);
%     Cl2 = (1/d22)*sum(D2,2);
%     Cl3 = (1/d33)*sum(D3,2);
%     oldCoh = newCoh;
%     x = [k1 k2 k3];
%     
%     for i = x
% 
%         d1 = Cl1 - X(:,i);
%         d1 = norm(d1);
%         d2 = Cl2 - X(:,i);
%         d2 = norm(d2);
%         d3 = Cl3 - X(:,i);
%         d3 = norm(d3);
%         %Look for the minimum distance
%         if d1 <= d2 
%             if d1 <= d3
%                 CS1 = [CS1,i];
%             else 
%                 CS3 = [CS3,i];
%             end
%         else
%             if d2 < d3
%                 CS2 = [CS2, i];
%             else
%                 CS3 = [CS3, i];
%             end
%         end
%     end
%     D1 = X(:,CS1);
%     D2 = X(:,CS2);
%     D3 = X(:,CS3);
%     [x11, d11] = size(D1);
%     [x22, d22] = size(D2);
%     [x33, d33] = size(D3);
%     Cl1 = (1/d11)*sum(D1,2);
%     Cl2 = (1/d22)*sum(D2,2);
%     Cl3 = (1/d33)*sum(D3,2);
%     n1=norm((D1 - Cl1*ones(1,d11)),'fro'); 
%     n2=norm((D2 - Cl2*ones(1,d22)),'fro');
%     n3=norm((D3 - Cl3*ones(1,d33)),'fro');
%     newCoh = n1+n2+n3;
%     coh=[coh,newCoh];
% end
% figure(3)
% hold on;
% plot(X(3,CS1),X(4,CS1),'* g',X(3,CS2),X(4,CS2),'* r',X(3,CS3),...
%     X(4,CS3), '* b','LineWidth', 40)
% plot(Cl1(3),Cl1(4), 'x g', Cl2(3),Cl2(4),...
%      'x r',  Cl3(3), Cl3(4), 'x b' ) 
% 
% leg=legend('Setosa', 'Virginica', 'Versacolor','Setosa-centroid',...
%     'Virginica-centroid', 'Versacolor-centroid', 'Location','northwest');
% xlabel('Petal Length', 'FontSize', 20); 
% ylabel('Petal Width', 'FontSize', 20);
% title('K-means Iris Prediction', 'FontSize', 20);
% figure(4)
% hold on;
% plot(coh(1,:),'x b','LineWidth',40)
% xlabel('Iterations', 'FontSize', 20); 
% ylabel('Coherence', 'FontSize', 20);
% title('Iris Coherence Plot k-means','FontSize', 20)


%%%Medoid
clear
close all
load IrisDataAnnotated
[nrow,ncol]=size(X);
%rng(1);
k = randperm(ncol);
k1 = k(1);
k2 = k(2);
k3 = k(3);
coh=[];
dist=NaN(ncol,ncol); %distance matrix
for i = 1:ncol
    for j = 1:ncol
        dist(i,j) = norm(X(:,i)-X(:,j),1);
    end
end

medoid1=X(:,k1);
medoid2=X(:,k2);
medoid3=X(:,k3);
oldCoh=10000;
newCoh=5000;
while((oldCoh-newCoh)>0.1)
    oldCoh=newCoh;
    fprintf('Coherence new %d old %d\n',newCoh,oldCoh);
    size(medoid1);
    x1=X-(medoid1*(ones(1,ncol)));
    x2=X-(medoid2*(ones(1,ncol)));
    x3=X-(medoid3*(ones(1,ncol)));
    CS1 = []; Cl1 = [];
    CS2 = []; Cl2 = [];
    CS3 = []; Cl3 = [];
    for i = 1:150
        if (norm(x1(:,i),1) <= norm(x2(:,i),1))
            if(norm(x1(:,i),1) <= norm(x3(:,i),1))
               CS1=[CS1,X(:,i)];
               Cl1=[Cl1,i];
            else
               CS3=[CS3,X(:,i)];
               Cl3=[Cl3,i];
            end
        else
            if(norm(x2(:,i),1) <= norm(x3(:,i),1))
               CS2=[CS2,X(:,i)];
               Cl2=[Cl2,i];
            else
               CS3=[CS3,X(:,i)];
               Cl3=[Cl3,i];
            end
        end
    end


    clus1=sum(dist(Cl1,Cl1),2);
    [M1,I1] = min(clus1);
    clus2=sum(dist(Cl2,Cl2),2);
    [M2,I2] = min(clus2);
    clus3=sum(dist(Cl3,Cl3),2);
    [M3,I3] = min(clus3);
    newCoh=M1+M2+M3;
    coh = [coh, newCoh];
    medoid1=X(:,Cl1(I1));
    medoid2=X(:,Cl2(I2));
    medoid3=X(:,Cl3(I3));
end
figure(1)
hold on;
plot(CS1(3,:),CS1(4,:),'* g',CS2(3,:),CS2(4,:),'* r',...
    CS3(3,:),CS3(4,:), '* b', 'LineWidth',40)
plot(medoid1(3),medoid1(4), 'x g', medoid2(3),medoid2(4),...
    'x r',  medoid3(3), medoid3(4), 'x b','MarkerSize', 20) 
leg=legend('Virginica', 'Setosa', 'Versacolor', 'Virginica Medoid',...
    'Setosa Medoid', 'Versacolor Medoid','Location','northwest'); 
set(leg, 'FontSize', 16); 
set(gca, 'FontSize', 14); 
xlabel('Petal Length', 'FontSize', 20); 
ylabel('Petal Length', 'FontSize', 20);
title('K-medoid iris prediction', 'FontSize', 20);
figure(4)
 hold on;
 plot(coh(1,:),'x b','LineWidth',40)
 xlabel('Iterations', 'FontSize', 20); 
 ylabel('Coherence', 'FontSize', 20);
 title('Coherence Plot k-medoid','FontSize', 20)


%%%%%Answer 2

clear
load BiopsyDataAnnotated.mat
%For NaN identification
[row, col] = find(isnan(X));
for i = col
    X(:,i)=[];
   fprintf('col %d\n',i) 
end
[nrow,ncol]=size(X);
dist=NaN(ncol, ncol);
n=ncol;
k = randperm(n);
k1 = k(1);
k2 = k(2);
%k3 = k(3);
coh=[];

for i = 1:ncol
    for j = 1:ncol
        dist(i,j) = norm(X(:,i)-X(:,j),1);
    end
end

medoid1=X(:,k1);
medoid2=X(:,k2);
%medoid3=X(:,k3);
oldCoh=100000000;
newCoh=500000;
while((oldCoh-newCoh)>0.1)
    oldCoh=newCoh;
    fprintf('Coherence new %d old %d\n',newCoh,oldCoh);
    size(medoid1);
    x1=X-(medoid1*(ones(1,ncol)));
    x2=X-(medoid2*(ones(1,ncol)));
    %x3=X-(medoid3*(ones(1,ncol)));
    CS1 = []; Cl1 = [];
    CS2 = []; Cl2 = [];
    %CS3 = []; Cl3 = [];
    for i = 1:ncol
        if (norm(x1(:,i),1) <= norm(x2(:,i),1))
            %if(norm(x1(:,i)) <= norm(x3(:,i)))
           CS1=[CS1,X(:,i)];
           Cl1=[Cl1,i];
        else
               CS2=[CS2,X(:,i)];
               Cl2=[Cl2,i];
        end
    end
    clus1=sum(dist(Cl1,Cl1),2);
    [M1,I1] = min(clus1);
    clus2=sum(dist(Cl2,Cl2),2);
    [M2,I2] = min(clus2);
    newCoh=M1+M2;%+M3;
    fprintf('Coherence new %d old %d\n',newCoh,oldCoh);
    coh = [coh, newCoh];
    medoid1=X(:,Cl1(I1));
    medoid2=X(:,Cl2(I2));
    %medoid3=X(:,Cl3(I3));
end
figure(10)
 hold on;
 plot(coh(1,:),'x b','LineWidth',40)
 xlabel('Iterations', 'FontSize', 20); 
 ylabel('Coherence', 'FontSize', 20);
 title('Coherence Plot k-medoid for Biopsy data','FontSize', 20)



%%%%Answer 3
clear
load CongressionalVote.mat
[row, col] = find(isnan(X));
for i = col
    X(:,i)=[];
   fprintf('col %d\n',i) 
end
[nrow,ncol]=size(X);
dist=zeros(ncol, ncol);
vLen=length(X(:,1));
for i = 1:ncol
    for j = 1:ncol
        num=0;den=0;
        for q = 1:vLen
            if(X(q,i) == 0 || X(q,j)== 0)
                continue;
            elseif(X(q,i) ~= X(q,j))
                num=num+1;
            else
                den=den+1;
            end
        end
        if(num ~= 0 || den ~= 0)
            dist(i,j) = num/(num+den);
        end
    end
end

k = randperm(ncol);
k1 = k(1);
k2 = k(2);
coh=[];

medoid1=X(:,k1);
medoid2=X(:,k2);
oldCoh=100000000;
newCoh=500000;
while((oldCoh-newCoh)>0.1)
    oldCoh=newCoh;
    fprintf('Coherence new %d old %d\n',newCoh,oldCoh);
    size(medoid1);
    x1=X-(medoid1*(ones(1,ncol)));
    x2=X-(medoid2*(ones(1,ncol)));
    CS1 = []; Cl1 = [];
    CS2 = []; Cl2 = [];
    for i = 1:ncol
        if (norm(x1(:,i),1) <= norm(x2(:,i),1))
           CS1=[CS1,X(:,i)];
           Cl1=[Cl1,i];
        else
               CS2=[CS2,X(:,i)];
               Cl2=[Cl2,i];
        end
    end
    clus1=sum(dist(Cl1,Cl1),2);
    [M1,I1] = min(clus1);
    clus2=sum(dist(Cl2,Cl2),2);
    [M2,I2] = min(clus2);
    newCoh=M1+M2;%+M3;
    fprintf('Coherence new %d old %d\n',newCoh,oldCoh);
    coh = [coh, newCoh];
    medoid1=X(:,Cl1(I1));
    medoid2=X(:,Cl2(I2));
    
end
% 
% figure(10)
%   hold on;
%   plot(coh(1,:),'x b','LineWidth',40)
%   xlabel('Iterations', 'FontSize', 20); 
%   ylabel('Coherence', 'FontSize', 20);
%   title('Coherence Plot k-medoid for Congressional Vote data','FontSize', 20)