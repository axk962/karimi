function hw4_math4443()
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%Main Function to call all other Function%%%%%%
    clear
    close all
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%Answer 1 Call%%%%%%%%%%%%%%%%%%%%%%%%%%
    load HandwrittenDigitsTestset.mat
    XT = X;
    IT = I;
    load HandwrittenDigits.mat
    global succ ;
    succ = zeros(21,2); %saves success rate at diffrent m for test & train

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%Answer 2 Call%%%%%%%%%%%%%%%%%%%
    clear
    close all
    load HandwrittenDigits.mat    
    load HandwrittenDigitsTestset.mat    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%Answer 3 Call%%%%%%%%%%%%%%%%%%%
    clear
    close all
    load ForestSpectraTest.mat
    XT = X;
    ItypeT = Itype;
    namesT = names;
    load ForestSpectra.mat
    fs_p3(X,Itype,names,XT,ItypeT,namesT);
end
%%%%%%%%%%%%%%%%%%%%%1%%%%%%%%%%%%%%%%%%%%%
function pca_p1(X,I,fc,XT,IT)
    
    [nrow, ncol]=size(X);
    [nrowt, ncolt]=size(XT);
    %%Training
    anotMat=zeros(10,ncol);
    argminMat=NaN(10,ncol);
    resultMatrix=zeros(11,ncol);
    resultMatrix(1,:)=I;
    %%Test
    anotMatT=zeros(10,ncolt);
    argminMatT=NaN(10,ncolt);
    resultMatrixT=zeros(11,ncolt);
    resultMatrixT(1,:)=IT;
    
    rc=2;
    for m = 5:5:100    
        i=1;
        for k=[1,2,3,4,5,6,7,8,9,0]
        %for k = 0:1:9
            k1=find(k==I);
            %X1=X(:,k1);
            j=1;
            for c = k1
                anotMat(i,j)=c;
                j=j+1;
            end
            nz=nonzeros(anotMat(i,:));
            [u, d, v]=svd(X(:,nz));
            diag(d);

            prjt=u(:,1:m)*u(:,1:m)';
            %minMat=sum(X(:,nz)-prjt*X(:,nz));
            minMat = sum((X-(prjt*X)).^2);
            minMatT = sum((XT-(prjt*XT)).^2);
            argminMat(i,:) = minMat;
            argminMatT(i,:) = minMatT;
            i=i+1;
        end

        [~, minIndexCol] = min(argminMat);
        resultMatrix(rc,:) = minIndexCol;
        
        [~, minIndexColT] = min(argminMatT);
        resultMatrixT(rc,:) = minIndexColT;
        
        rc=rc+1;
    end
    resultMatrix( resultMatrix==10 )=0;
    resultMatrixT( resultMatrixT==10 )=0;    
    %succ=zeros(21,1);
     for pr = 1:21
         succ(pr,1)= sum(resultMatrix(1,:)==resultMatrix(pr,:))/ncol;
         succ(pr,2)= sum(resultMatrixT(1,:)==resultMatrixT(pr,:))/ncolt;
     end

    figure(1)
    hold on;
    plot(5:5:100,succ(2:21,fc),'r-x' ,'MarkerSize',10)
    plot(5:5:100,succ(2:21,2), 'b-x', 'MarkerSize',10)

    title( sprintf('Sucess rate of training and test set for PCA'))
    xlabel('Feature Vectors', 'FontSize', 15); 
    ylabel('Success Frequency', 'FontSize', 15); 
    legend('Training','Test','Location','southwest')
    set(gca, 'FontSize', 14); 
end
%%%%%%%%%%%%%%%%%%%%%2%%%%%%%%%%%%%%%%%%%%%
function knn_p2(X,I,fc)

    [nrow, ncol]=size(X);

    dist=NaN(ncol,ncol);
    knn=NaN(ncol,ncol);
    for i = 1:ncol
       for j = 1:ncol
          dist(i,j)= norm(X(:,i)-X(:,j)); 
       end
    end
    %knn1=NaN(1707,1707);
    %Create knn matrix
    count=1;
    for count = 1:ncol
        a=sort(dist(:,count),1,'ascend');
        for l1 = 1:ncol
            for l2 = 1:ncol
                if (a(l1)== dist(l2,count))
                    knn(l1,count)=l2;
                end
            end
        end
    end
    resultMatrix=NaN(30,ncol);
    for k = 1:30
        freqMat=zeros(10,ncol);
        for p = 1:1707
            for k1 = 1:k
                anotVal=knn(k1,p);
                digit = I(anotVal);
                if (digit == 0)
                    freqMat(10,p)= freqMat(10,p)+1;
                else
                    freqMat(digit,p)= freqMat(digit,p)+1;
                end
            end
        end
        [maxNumCol, maxIndexCol] = max(freqMat);
        resultMatrix(k,:) = maxIndexCol;
    end

    succ=zeros(30,1);
    for pr = 1:30
        succ(pr)= sum(resultMatrix(1,:)==resultMatrix(pr,:));
    end
    if fc == 1
        figure(1)
        hold on;
        plot(1:30,succ(1:30,1), 'b-x', 'MarkerSize',10)
    else
        plot(1:30,succ(1:30,1), 'r-x', 'MarkerSize',10)
    end
    if fc == 2
        title( sprintf('KNN at different k-values'))
        xlabel('Number of K Nearest Neighbor', 'FontSize', 15); 
        ylabel('Number of Correct predictions', 'FontSize', 15); 
        legend('Training','Test','Location','northeast')
        set(gca, 'FontSize', 14); 
     end    
end


%%%%%%%%%%%%%%%%%%%%%%%3%%%%%%%%%%%%%%%%%%%%%
function fs_p3(X,Itype,names,XT,ItypeT,namesT)
    %clear
    %load ForestSpectra.mat
    [nrow, ncol]=size(X);
    anotMat=zeros(4,ncol);
    cm = zeros(50,5); %cm = Cluster Matrix having centroid of four clusters
                                         %and global centroid
    cm(:,5)= (sum(X,2))/ncol;  %Global Centroid
    SW = zeros(nrow,nrow); %within Cluster Matrix
    SB = zeros(nrow,nrow); %Between Cluster Matrix
    for k=1:4
        ci = find(k==Itype); %ci = Cluster Indices
        j=1;
        for c = ci
            anotMat(k,j)= c;
            j=j+1;
        end
        cm(:,k)=(sum(X(:,ci),2))/length(ci);
        
        SW=SW+(X(:,ci)-cm(:,k))*(X(:,ci)-cm(:,k))';
        SB = SB+length(ci)*((cm(:,k)-cm(:,5))*(cm(:,k)-cm(:,5))');
    end
    
    delta=0.001;
    inverseMatrix=(SW+(delta*eye(length(SW))))\SB;
    [Q, D]=eig(inverseMatrix);
    
    Z=Q(:,1:4)'*X;
    %Plot of data using LDA direction
    figc=1;
    for i = 1:4
        for j = i+1:4
            figure(figc)
            %plot(Z(i,:),Z(j,:),'k.', 'MarkerSize',25)
            plot(Z(i,1==Itype),Z(j,1==Itype),'. b',...
                    Z(i,2==Itype),Z(j,2==Itype),'. g',...
                    Z(i,3==Itype),Z(j,3==Itype),'. r',...
                    Z(i,4==Itype),Z(j,4==Itype),'. y',...
                    'MarkerSize',10)
            title( sprintf('Plot on LDA direction Training Set'))
            xlabel(['LDA direction ', num2str(i)])
            ylabel(['LDA direction ', num2str(j)])
            axis('equal')
            set(gca,'FontSize',20)
            figc = figc+1;
        end
    end
    
    %load load ForestSpectraTest.mat
    Z=Q(:,1:4)'*XT;
    %Plot of data using LDA direction
    figc=11;
    for i = 1:4
        for j = i+1:4
            figure(figc)
            %plot(Z(i,:),Z(j,:),'k.', 'MarkerSize',25)
            plot(Z(i,1==ItypeT),Z(j,1==ItypeT),'. b',...
                    Z(i,2==ItypeT),Z(j,2==ItypeT),'. g',...
                    Z(i,3==ItypeT),Z(j,3==ItypeT),'. r',...
                    Z(i,4==ItypeT),Z(j,4==ItypeT),'. y',...
                    'MarkerSize',10)
            title( sprintf('Plot on LDA direction Test Set'))
            xlabel(['LDA direction ', num2str(i)])
            ylabel(['LDA direction ', num2str(j)])
            axis('equal')
            set(gca,'FontSize',20)
            figc = figc+1;
        end
    end
end

