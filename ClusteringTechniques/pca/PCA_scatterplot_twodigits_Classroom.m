%PCA scatter plot of two different digits


load HandwrittenDigits X I
% Select the digits k1 and k2
k1 = 3;
k2 = 8;
I1 = find(I == k1);
L1 = length(I1);
I2 = find(I == k2);
L2 = length(I2);
Y1 = X(:,I1);
Y2 = X(:,I2);
Y = [Y1,Y2];
% Center the data
y_c = 1/(L1+L2)*sum(Y(:,:),2);
Y_c = Y - y_c*ones(1,L1+L2);
% PCA
[U,D,V] = svd(Y_c);
% First two principal components
Z = U(:,1:3)'*Y_c;
figure(1)
% Plotting k1's in red, k2's in blue
plot(Z(1,1:L1),Z(2,1:L1),'r.','MarkerSize',20)
hold on
plot(Z(1,L1+1:end),Z(2,L1+1:end),'b.','MarkerSize',20)
hold off
set(gca,'FontSize',20)
xlabel('Component on u_1','FontSize',20)
ylabel('Component on u_2','FontSize',20)

% Feature vectors

figure(2)
subplot(1,2,1)
imagesc(reshape(U(:,1),16,16)')
colormap(gray);
axis('square')
axis('off')
title('u_1','FontSize',20)
subplot(1,2,2)
imagesc(reshape(U(:,2),16,16)')
colormap(gray);
axis('square')
axis('off')
title('u_2','FontSize',20)

figure(3)
% Plotting k1's in red, k2's in blue
plot3(Z(1,1:L1),Z(2,1:L1),Z(3,1:L1),'r.','MarkerSize',20)
hold on
plot3(Z(1,L1+1:end),Z(2,L1+1:end),Z(3,L1+1:end),'b.','MarkerSize',20)
hold off
set(gca,'FontSize',20)


