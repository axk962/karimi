% This program demonstrates how the PCA is capable of identifying hidden
% redundancies in data. The data consist of realizations of a multivariate
% r.v. in 3 dimensions of the form
%  
%                     [  y1        ]
%  x =  x0 +   [ y1 + y2]  
%                     [ y1 - y2 ]
% 
% where y1 and y2 are uniformly distributed. The third singular value
% corresponding to the direction of redundancy should vanish, and the
% projection onto the first two singular vectors shows the reduced variable.
%
close all
p  = 2500;
y1 = 2*rand(1,p);
y2 = rand(1,p);
X  =[4;4;-3]*ones(1,p) +  [y1;y1+y2;y1-y2];

% Center the data
xbar = 1/p*sum(X,2);
Xc = X - xbar*ones(1,p);

figure(1)
plot3(X(1,:),X(2,:),X(3,:),'k.','MarkerSize',10)
set(gca,'FontSize',20);
%pause

[U,D,V] = svd(Xc);

% Singular values

sigma = diag(D)
%pause

% Project onto the first two directions

Z = U(:,1:2)'*Xc;

figure(2)

plot(Z(1,:),Z(2,:),'k.','MarkerSize',10)
set(gca,'FontSize',20)

%pause
% What happens if we do not center the data?

[U,D,V] = svd(X);

% Singular values

sigma = diag(D)
%pause

% Project onto the first two directions

Z = U(:,1:2)'*X;

figure(3)

plot(Z(1,:),Z(2,:),'k.','MarkerSize',10)
set(gca,'FontSize',20)



