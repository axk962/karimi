% SOM demo. The program generates a data set in three dimensions consisting 
% of three separate cluster. The SOM algorithm is used to map the data down 
% on two dimensions so that like data points (= belonging to the same
% group) end up close to each others.
%--------------------------------------------------------------------------
%
% Generating data: three clusters of points in 3-space
%--------------------------------------------------------------------------
close all
n = 100;  % number of points in each cluster
th = pi/2*rand(1,n);
phi = pi/2*rand(1,n);
r = ones(1,100) + 0.02*randn(1,100);
X1 = [r.*sin(th).*cos(phi);r.*sin(th).*sin(phi);r.*cos(th)];
th = pi/2*rand(1,n);
phi = pi/2*ones(1,100) + pi/2*rand(1,n);
X2 = [r.*sin(th).*cos(phi);r.*sin(th).*sin(phi);r.*cos(th)];
th =  pi/2*rand(1,n);
phi = pi*ones(1,100) + pi/2*rand(1,n);
X3 = [r.*sin(th).*cos(phi);r.*sin(th).*sin(phi);r.*cos(th)];


figure(1)
plot3(X1(1,:),X1(2,:),X1(3,:),'r.','MarkerSize',20)
hold on
plot3(X2(1,:),X2(2,:),X2(3,:),'b.','MarkerSize',20)
plot3(X3(1,:),X3(2,:),X3(3,:),'g.','MarkerSize',20)
hold off


% Randomizing the sample.
X = [X1,X2,X3];
nx = 3*n;
X = X(:,randperm(nx));

%--------------------------------------------------------------------------
% Defining the map and corresponding prototype vectors. The map Q consists
% of 10x10 points in a rectangular grid in two-dimensional space.
%--------------------------------------------------------------------------
% Map
q1 = [1:10]'*ones(1,10);
q2 = ones(10,1)*[1:10];
Q = [q1(:) q2(:)];
% Define the distance squared matrix
D2 = zeros(100,100);
for j = 1:100
    for k = 1:100
        D2(j,k) = norm(Q(j,:) - Q(k,:))^2;
        %D2(k,j) = D(j,k);
    end
end


% Prototypes
m =  0.1*rand(3,100);
% to see that the prototypes are originally randomly ordered, plot a
% colormap of the x-, y-, and z-coordinates of the prototypes. Later on,
% these plots should have more structure.
figure(3)
subplot(1,3,1)
imagesc(reshape(m(1,:)',10,10))
axis('square')
 title('x-coords','FontSize',20)
subplot(1,3,2)
imagesc(reshape(m(2,:)',10,10))
axis('square')
 title('y-coords','FontSize',20)
subplot(1,3,3)
imagesc(reshape(m(3,:)',10,10))
axis('square')
 title('z-coords','FontSize',20)
pause
%--------------------------------------------------------------------------
% Definining and adjacency matrix of the map elements. This is not part of
% the SOM algorithm, and is here only for plotting purposes in this demo.
%--------------------------------------------------------------------------
rows = [];
cols = [];
vals = [];
ell = 10;
for j = 1:ell
    for k = 1:ell
        jk = j + (k-1)*ell;
        if k>1
            % add left neighbor
            rows = [rows, jk];
            cols = [cols, jk - ell];
            vals = [vals, 1];
        end
        if k<ell
            % add right neighbor
            rows = [rows, jk];
            cols = [cols, jk + ell];
            vals = [vals, 1];
        end
        if j>1
            % add top neighbor
            rows = [rows, jk];
            cols = [cols, jk - 1];
            vals = [vals, 1];
        end
        if j<ell
            % add bottom neighbor
            rows = [rows,jk];
            cols = [cols, jk + 1];
            vals = [vals, 1];
        end
    end
end
A = sparse(rows,cols,vals);
%--------------------------------------------------------------------------
% SOM algorithm: Specifying the learning rate and naighborhood map. In this
% version, the learning rate is constant, while the neighborhood map is a
% Gaussian.
% The stopping criterion is given as maximum number of epochs.
%--------------------------------------------------------------------------
alpha = 0.2; % Learning rate
gamma = 1.5;  % length scale in the map space defining the neighborhood map
epochs = 6;

% Running the learning algorithm
for ne = 1:epochs
    for j = 1:nx
        % Find the closest prototype
        pdist2 = sum((m - X(:,j)*ones(1,100)).^2);
        [dc2,ic] = min(pdist2);
        % Compute the map distances from the closest map point
        mdist2 = D2(:,ic);
        h = exp( - 1/(2*gamma^2)*mdist2); 
        m = m + alpha*(ones(3,1)*h').*(X(:,j)*ones(1,100) - m);
        % Plotting the prototype vectors as a grid (This is not part of the
        % algorithm, just a part of the demo)
        figure(2)
        % Plot the data
        plot3(X1(1,:),X1(2,:),X1(3,:),'r.','MarkerSize',20)
        hold on
        plot3(X2(1,:),X2(2,:),X2(3,:),'b.','MarkerSize',20)
        plot3(X3(1,:),X3(2,:),X3(3,:),'g.','MarkerSize',20)
        % Plot prototypes
        plot3(m(1,:),m(2,:),m(3,:),'k.','MarkerSize',15)
        hold off
        if j == nx
            % At the end of the epoch, plot the coordinates
           figure(3)
           subplot(1,3,1)
           imagesc(reshape(m(1,:)',10,10))
           axis('square')
           title('x-coords','FontSize',20)
           subplot(1,3,2)
           imagesc(reshape(m(2,:)',10,10))
           axis('square')
           title('y-coords','FontSize',20)
           subplot(1,3,3)
           imagesc(reshape(m(3,:)',10,10))
           axis('square')
           title('z-coords','FontSize',20)
           drawnow
        end
    end
    
    % To emphasize the net structure of prototypes, add the mesh using the
    % adjacency matrix
    figure(2)
    hold on
    for j = 1:ell^2-1
        I = find(A(j,:)>0);
        for k = 1:length(I)
            plot3([m(1,j),m(1,I(k))],[m(2,j),m(2,I(k))],[m(3,j),m(3,I(k))],'k-');
        end
    end
   
    % Visualizing the data: For every data point, find the nearest prototype and
    % plot a dot in the map randomly positioned in the vicinity of the
    % coordinates of the prototype

    figure(4)
    % Plotting he buttons on the map
    thplot = linspace(0,2*pi,100);
    cc = cos(thplot);
    ss = sin(thplot);
    for j = 1:100
        plot(Q(j,1)*ones(1,100) + 0.5*cc,Q(j,2)*ones(1,100)+0.5*ss,'k-','LineWidth',2)
        hold on
    end
    axis('square')
    axis([0,11,0,11])
    set(gca,'FontSize',20)
    for j = 1:n
        % finding the nearest prototype
        d = sum((m - X1(:,j)*ones(1,100)).^2,1);
        [mv,i] = min(d);
        % Plotting a random dot in the button corresponding to the nearest
        % prototype
        r  = 0.4*rand;
        th = 2*pi*rand;
        plot((Q(i,1)+r*cos(th)),(Q(i,2)+r*sin(th)),'r.','MarkerSize',20)
        % finding the nearest prototype
        d = sum((m - X2(:,j)*ones(1,100)).^2,1);
        [mv,i] = min(d);
        % Plotting a random dot in the button corresponding to the nearest
        % prototype
        r  = 0.4*rand;
        th = 2*pi*rand;
        plot((Q(i,1)+r*cos(th)),(Q(i,2)+r*sin(th)),'b.','MarkerSize',20)
        % finding the nearest prototype    
        d = sum((m - X3(:,j)*ones(1,100)).^2,1);
        [mv,i] = min(d);
        % Plotting a random dot in the button corresponding to the nearest
        % prototype
        r  = 0.4*rand;
        th = 2*pi*rand;
        plot((Q(i,1)+r*cos(th)),(Q(i,2)+r*sin(th)),'g.','MarkerSize',20)
    end
    title(['Epoch = ' num2str(ne)],'FontSize',20)
    hold off
    pause
end

