close all
% Snake demo

% Generating the data that follows approximately a one-dimensional curve in
% the plane

t = -2.5:0.1:2.5;
x = (t - 1).*t.*(t + 1);
y = 0.2*(t-2).*(t+2).*t;

figure(1)
plot(x,y,'k--','LineWidth',2)
set(gca,'FontSize',20)

snake = @(t) [(t - 1).*t.*(t + 1); 0.2*(t-2).*(t+2).*t];

p = 100;
xi = 5*(0.5*ones(1,p) - rand(1,p));
X = snake(xi);
X = X + 0.05*randn(2,p);

figure(1)
hold on
plot(X(1,:),X(2,:),'r.','MarkerSize',20)
title('Data','FontSize',20)
hold off
set(gca,'FontSize',20)
pause

% Defining the coordinate space and initializing the snake

% PCA initialization
c = 1/p*sum(X,2);
Xc = X - c*ones(1,p);
[U,D,V] = svd(Xc);
z = U(:,1)'*Xc;
zmin = min(z);
zmax = max(z);
k = 30;
t = zmin + (zmax-zmin)/(k-1)*(0:k-1);
M = c*ones(1,k) + U(:,1)*t;


k = 30;
q = 1:k;

% Distance matrix of the coordinates
[Q1,Q2] = meshgrid(q,q);
D2 = (Q1 - Q2).^2;

epochs = 4; % Number of rounds with all data
alpha0 = 0.3; % initial learning rate
gamma = 2; % Width parameter of the neighborhood function
H = exp( - 1/(2*gamma^2)*D2);

figure(2)

count = 0;
maxiter = epochs*p;
for n_outer = 1:epochs
    for j = 1:p
        count = count + 1;
        x = X(:,j); % Current data point
        % Find the closest prototype vector
        dist2 = sum((M - x*ones(1,k)).^2);
        [d,ell] = min(dist2);
        h = H(ell, :);
        dM = x*ones(1,k) - M;
        alpha = alpha0*(1 - count/maxiter);
        M = M + alpha*(ones(2,1)*h).*dM;
        figure(3)
        plot(X(1,:),X(2,:),'r.','MarkerSize',20)
        hold on
        plot(X(1,j),X(2,j),'g.','MarkerSize',25)
        plot(M(1,:),M(2,:),'k.','MarkerSize',20)
        plot(M(1,:),M(2,:),'k-','LineWidth',2)
        set(gca,'FontSize',20)
        if (n_outer == 1) & (j == 1)
            pause
        else
           pause(0.05)
        end
    hold off
    end
    pause
end

