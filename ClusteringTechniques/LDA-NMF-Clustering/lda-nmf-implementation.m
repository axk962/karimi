%%1%%

clear 
close all

load WineData.mat

k1=find(I==1);
k2=find(I==2);
k3=find(I==3);
[nr,nc]=size(X);
G_Cnt=(sum(X,2))/nc;

D1=X(:,k1);
D2=X(:,k2);
D3=X(:,k3);

D1_Cnt=(sum(D1,2))/length(k1);
D2_Cnt=(sum(D2,2))/length(k2);
D3_Cnt=(sum(D3,2))/length(k3);

S1=(D1-D1_Cnt*ones(1,length(D1)))*(D1-D1_Cnt*ones(1,length(D1)))';
S2=(D2-D2_Cnt*ones(1,length(D2)))*(D2-D2_Cnt*ones(1,length(D2)))';
S3=(D3-D3_Cnt*ones(1,length(D3)))*(D3-D3_Cnt*ones(1,length(D3)))';

SW=S1+S2+S3;

SB1=length(D1)*((D1_Cnt-G_Cnt)*(D1_Cnt-G_Cnt)');
SB2=length(D2)*((D2_Cnt-G_Cnt)*(D2_Cnt-G_Cnt)');
SB3=length(D3)*((D3_Cnt-G_Cnt)*(D3_Cnt-G_Cnt)');

SB=SB1 + SB2 + SB3;

%As a safety, I use deltaI, even when the Sw Matrix is invertible.
delta=0.001;
inverseMatrix=(SW+(delta*eye(length(SW))))\SB;
[Q, D]=eig(inverseMatrix);
%Principle Components
Z=Q(:,1:3)'*X;
figure(1)
%Plot of data using principal component
plot(Z(2,:),Z(3,:),'k.', 'MarkerSize',20)
title( sprintf('Plot on LDA principal direction'))
xlabel(['Principal direction 2'])
ylabel(['Principal direction 3'])
%axis('equal')
set(gca,'FontSize',10)       

Pax=Q(:,1:3)*Z;
figure(4)
%Plot of data using principal component
plot3(Pax(1,:),Pax(2,:),Pax(3,:),'k.', 'MarkerSize',20)
title( sprintf('Plot of Projection on principal component')...
    , 'FontSize', 10)
xlabel(['Direction 1'])
ylabel(['Direction 2'])
zlabel(['Direction 3'])
%axis('equal')
set(gca,'FontSize',10)       

%%%%%2%%%%%
clear
clc
close all
load HandwrittenDigits.mat

k0=find(I==0);
k4=find(I==4);

D1=X(:,k0);
D2=X(:,k4);

Data=[D1 D2];
[nr nc]=size(Data);
G_Cnt=(sum(Data,2))/nc;

D1_Cnt=(sum(D1,2))/length(k0);
D2_Cnt=(sum(D2,2))/length(k4);

S1=(D1-D1_Cnt*ones(1,length(k0)))*(D1-D1_Cnt*ones(1,length(k0)))';
S2=(D2-D2_Cnt*ones(1,length(k4)))*(D2-D2_Cnt*ones(1,length(k4)))';
SW=S1+S2;

SB1=((D1-G_Cnt)*(D1-G_Cnt)');
SB2=((D2-G_Cnt)*(D2-G_Cnt)');
SB=SB1 + SB2;
%%chol(Sb)
delta=0.001;
inverseMatrix=(SW+(delta*eye(length(SW))))\SB;
[Q, D]=eig(inverseMatrix);

Z=Q(:,1:2)'*Data;
figure(1);
plot(Z(1,:),Z(2,:),'k.', 'MarkerSize',4)

title( sprintf('Plot on LDA principal direction'))
xlabel(['Principal Direction 1'])
ylabel(['Principal Direction 2'])
set(gca,'FontSize',10)      



%%ANLS%%
close all
clear

load ../Math-444-HW3/HandwrittenDigits.mat
k0=find(I==0);
k4=find(I==4);
D1=X(:,k0);
D2=X(:,k4);
X_04=[D1 D2];
rng(2);
X_04=X_04(:,randperm(size(X_04,2)));
[U, D, V]=svd(X_04);
imgCounter=1;
figure(10)
hold on;

for k = [5 10 20]
     
    convergence1 = 1;
    convergence2 = 10;
    counter = 1;
    while abs(convergence2-convergence1) > 0.1
        convergence1=convergence2;
        H0=D*V';
        H0=H0(1:k,:);
        W0=rand(256,k);
        for i=1:size(X_04,2)
            H1(:,i)=lsqnonneg(W0,X_04(:,i));
        end

        for i = 1 : size(X_04,1)
            W1(:,i)=lsqnonneg(H1', X_04(i,:)');
        end
        W1=W1';
        lambda = eye(k).*max(W1);
        W1=W1*inv(lambda);
        H1=lambda*H1;

        W_diff=norm(W0-W1, 'fro') / norm(W0, 'fro');
        H_diff=norm(H0-H1, 'fro') / norm(H0, 'fro');

        W0=W1;
        clear W1;

        H0=H1;
        clear H1;
        convergence2=W_diff+H_diff;
        counter=counter + 1;
    end

    X1 = W0*H0;
    subplot(1,3,imgCounter)
    imagesc(reshape(W0(:,5),16,16)');

    if imgCounter == 2
     title( sprintf('Plot of 0 with value of k = 5,  10 and 20'))   
     set(gca,'FontSize',15)
    end
    
    colormap(gray);
    axis('square')
    axis('off')
    imgCounter=imgCounter+1;
    
    H0(:,2)
end


figure(k+1)
imagesc(reshape(X_04(:,2),16,16)');
colormap(gray);
title( sprintf('Actual 0 plot from original data'))
axis('equal')
set(gca,'FontSize',15)       
print(k+1,'-dpng')    

                
                
