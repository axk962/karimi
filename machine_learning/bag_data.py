import numpy as np
import random
import pandas as pd

#bagging of the dataset
def bagsample(dataset, ratio=1.0):
    data_frame = False
    if isinstance(dataset, pd.DataFrame):
        col_name = dataset.columns.values
        dataset = np.array(dataset)
        data_frame = True
    sample = list()
    n_sample = round(len(dataset) * ratio)
    while len(sample) < n_sample:
        index = random.randrange(len(dataset))
        sample.append(dataset[index])
    sample = np.array(sample)
    if data_frame:
        sample = pd.DataFrame(sample)
        sample.columns = col_name
    return sample
