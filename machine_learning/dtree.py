import pandas as pd
import numpy as np
import pdb
from mldata import *
import operator
import math
import random

#pdb.set_trace()
decision_tree = {}
max_depth_count = 0


def getData(path):   
    #ml_dataset = parse_c45('example')
    file_name = path.split("/")[-1]
    file_path = '/'.join(path.split("/")[0:-1])+'/.'
    ml_dataset = parse_c45(file_name,file_path)
    col_len = len(ml_dataset[0])
    dataset_df = pd.DataFrame()
    temp_list = list()
    main_list = list()
    for i in range(0,len(ml_dataset)):
        main_list.append(ml_dataset[i][:])
    dataset_df = pd.DataFrame(main_list)
    dataset_df.columns = np.char.mod('f'+'%s',dataset_df.columns.values)

    return dataset_df

def informationGain(dataset,is_feature_numeric):
    #calculate data_points, i.e. number of data in parent node
    data_points = dataset.iloc[:,0].count()
    information_gain_list = list()
    split_point_list = list()
    for i in range(1, len(dataset.columns)-1): #last field is Label
        if is_feature_numeric[i-1]:
            information_gain, split_point = \
                informationGainContinuous(dataset,i,data_points)
            split_point_list.append(split_point)
        else:
            information_gain = \
                informationGainNominal(dataset,i,data_points)
            split_point_list.append(None)
        information_gain_list.append(information_gain) 
    return [information_gain_list, split_point_list]         
        

def GainRatioContinuous(dataset,index_num,data_points):
    pos_count = np.sum(dataset.iloc[:, -1])
    neg_count = np.sum(~dataset.iloc[:, -1])

    if neg_count == 0 or pos_count == 0:
        entropyParent = 0.0
    else:
        entropyParent = entropy(pos_count, neg_count)

    sub_entropy, split_point = informationGainContinuous(dataset, index_num, data_points)
    return [(sub_entropy / entropyParent), split_point]


def gainRatio(dataset, is_feature_numeric):
    # calculate data_points, i.e. number of data in parent node
    df_0_col = dataset.columns.values[0]
    data_points = dataset[df_0_col].count()
    gain_ratio_list = list()
    split_point_list = list()
    for i in range(1, len(dataset.columns) - 1):  # last field is Label
        if is_feature_numeric[i-1]:
            gain_ratio, split_point = \
                GainRatioContinuous(dataset, i, data_points)
            split_point_list.append(split_point)
        else:
            gain_ratio = \
                GainRatioNominal(dataset, i, data_points)
            split_point_list.append(None)
        gain_ratio_list.append(gain_ratio)
    return [gain_ratio_list, split_point_list]

def GainRatioNominal(dataset,index_num,data_points):
    pos_count = np.sum(dataset.iloc[:, -1])
    neg_count = np.sum(~dataset.iloc[:, -1])

    if neg_count == 0 or pos_count == 0:
        entropyParent = 0.0
    else:
        entropyParent = entropy(pos_count, neg_count)

    sub_entropy = informationGainNominal(dataset, index_num, data_points)

    return (sub_entropy / entropyParent)



def crossValidation(dataset_df,is_feature_numeric_list,ig,mxd):
    pos = []
    neg = []

    for i in range(0, dataset_df.shape[0]):
        if dataset_df.iloc[i, -1] == 1:
            pos.append(i)
        else:
            neg.append(i)
    df_0_col = dataset_df.columns.values[0]
    p = math.floor(0.2 * dataset_df[df_0_col].count())
    temp = float(len(pos))/dataset_df[df_0_col].count()*p
    if (temp - int(temp) >= 0.5):
        ppn = math.ceil(float(len(pos)) / dataset_df[df_0_col].count()* p)
    else:
        ppn = math.floor(float(len(pos)) / dataset_df[df_0_col].count()* p)

    random.seed(12345)
    random.shuffle(pos)
    random.shuffle(neg)

    l = list()
    c = 0
    while c < 5:
        cPos = ppn
        cNeg = p - ppn
        if c < 4:
            while cPos >= 1:
                l.append(dataset_df.iloc[pos[0], :])
                tmp = pos[0]
                pos.remove(tmp)
                cPos -= 1
            while cNeg >= 1:
                l.append(dataset_df.iloc[neg[0], :])
                tmp = neg[0]
                neg.remove(tmp)
                cNeg -= 1
        elif c == 4:
            while len(pos) > 0:
                l.append(dataset_df.iloc[pos[0], :])
                tmp = pos[0]
                pos.remove(tmp)
            while len(neg) > 0:
                l.append(dataset_df.iloc[neg[0], :])
                tmp = neg[0]
                neg.remove(tmp)
        c+=1
    acc_list = []
    newData = pd.DataFrame(l)
    for i in range(0, newData.shape[0], int(p)):
        tempNewD = pd.DataFrame(newData)
        subl = list()
        t = 0
        ite = i
        while t < p:
            subl.append(newData.iloc[ite, :])
            tempNewD.drop((int(newData.iloc[ite, 0])-1), inplace=True)
            t+= 1
            ite+=1
        testData = pd.DataFrame(subl)
        trainData = pd.DataFrame(tempNewD)
        testData = testData.reset_index().copy()
        recursive_node_function(None, None,\
            trainData,is_feature_numeric_list,0,ig)
        dec_tr = {}
        dec_tr = decision_tree
        if mxd != 0:
            dec_tr = reduced_max_depth(decision_tree,mxd)
        
        prediction_list = prediction_function(dec_tr,testData)
        acc = accuracy_fn(prediction_list,testData[testData.columns.values[-1]])
        acc_list.append(acc)
    avg_acc = sum(acc_list)/5
    return [avg_acc, dec_tr]

def accuracy_fn(lista,listb):
    count = 0
    for i in range(0,len(lista)):
        if lista[i] == listb[i]:
            count += 1
    acc = count/(len(lista)*1.0)
    return acc
    
def informationGainContinuous(dataset,index_num,data_points):
    partition_values_list = getPartitionOfContinuousFeature(dataset,index_num)
    weighted_child_entropy_list = list()
    if not partition_values_list: #check if the list is empty
        return [1.0, 'na']
    
    for i in range(0,len(partition_values_list)):
        temp_entropy = 0.0
        first_loop = True
        while True:
            if first_loop:
                subset = dataset[dataset.iloc[:,index_num] >  \
                         partition_values_list[i]]
            else :
                subset = dataset[dataset.iloc[:,index_num] <  \
                         partition_values_list[i]]
            pos_count = np.sum(subset.iloc[:,-1])
            neg_count = np.sum(~subset.iloc[:,-1])
            if neg_count == 0 or pos_count == 0:
                child_entropy = 0.0
            else:
                child_entropy = entropy(pos_count,neg_count)
            temp_entropy = temp_entropy + \
                ((pos_count+neg_count)*child_entropy) \
                / data_points
            if not first_loop:
                weighted_child_entropy_list.append(temp_entropy)
                break
            first_loop = False
    
    return [min(weighted_child_entropy_list), \
                partition_values_list[weighted_child_entropy_list.\
                index(min(weighted_child_entropy_list))]]

def getPartitionOfContinuousFeature(dataset,index_num):
    #sort the numeric column to get the partion values
    col_name = dataset.columns[index_num]
    temp_dataset = dataset.sort_values([col_name],ascending=True)
    uniq_cont_values = temp_dataset.iloc[:,index_num].unique()
    #dataset.reset_index(drop=True, inplace=True)
    #label_list = np.array(temp_dataset.iloc[:,-1])
    partition_points = list()
    for i in range(0 , len(uniq_cont_values) - 1):
        partition_points.append((uniq_cont_values[i]+\
            uniq_cont_values[i+1])/2)
    return partition_points
    
    
def informationGainNominal(dataset,index_num,data_points): #Non-numeric
    nominal_values = dataset.iloc[:,index_num].unique()
    weighted_child_entropy_list = list()
    for i in range(0, len(nominal_values)):
        #If condition to skip missing data "None"
        if nominal_values[i] is None or nominal_values[i] == "None":
            continue 
        data_subset = dataset[dataset.iloc[:,index_num] == nominal_values[i]]
        pos_count = np.sum(data_subset.iloc[:,-1])
        neg_count = np.sum(~data_subset.iloc[:,-1])
        if neg_count == 0 or pos_count == 0:
            child_entropy = 0.0
        else:
            child_entropy = entropy(pos_count,neg_count)
        #pos_count + neg_count = total data points in subset
        weighted_child_entropy = ((pos_count+neg_count)*child_entropy) \
                                     / data_points 
        weighted_child_entropy_list.append(weighted_child_entropy)
    return sum(weighted_child_entropy_list)
    

def entropy(pos_count,neg_count):
    neg_count = neg_count * 1.0 #convert to float
    pos_count = pos_count * 1.0
    entropy_value = (pos_count/(pos_count+neg_count) * \
                        np.log2((pos_count+neg_count)/pos_count)) + \
                        (neg_count/(pos_count+neg_count) * \
                        np.log2((pos_count+neg_count)/neg_count))
    return entropy_value    
    
        

#[dataset,is_feature_numeric_list] = getData()


def root_node_fn(dataset_root):    
    pos_count_r = np.sum(dataset_root.iloc[:,-1])
    neg_count_r = np.sum(~dataset_root.iloc[:,-1])
    if pos_count_r >= neg_count_r:
        maj_class_r = True
    else:
        maj_class_r = False
    
    if pos_count_r == 0:
        decision_tree[0]=[('root','None','None',maj_class_r,pos_count_r,neg_count_r,'Leaf',False)]
    elif neg_count_r == 0:
        decision_tree[0]=[('root','None','None',maj_class_r,pos_count_r,neg_count_r,'Leaf',True)]
    else:
        decision_tree[0]=[('root','None','None',maj_class_r,pos_count_r,neg_count_r,'NoLeaf',1)]

def recursive_node_function(information_gain_list, split_point_list,\
    dataset, is_feature_numeric_list,node_pointer,ig):
    global decision_tree
    if node_pointer == 0:
        decision_tree = {}
        root_node_fn(dataset)
        recursive_node_function(information_gain_list, split_point_list,\
            dataset,is_feature_numeric_list,1,ig)
        return decision_tree
    if ig == 0:    
        [information_gain_list, split_point_list] = \
            informationGain(dataset,is_feature_numeric_list)
    else:
        [information_gain_list, split_point_list] = \
            gainRatio(dataset,is_feature_numeric_list)

    split_point_na = [i for i, j in enumerate(split_point_list) if j == 'na']
    if split_point_na:
        split_point_list = [v for i, v in enumerate(split_point_list) if i not in split_point_na]
        information_gain_list = [v for i, v in enumerate(information_gain_list) if i not in split_point_na]
        is_feature_numeric_list = [v for i, v in enumerate(is_feature_numeric_list) if i not in split_point_na]

        del_dataset_col_index = [x+1 for x in split_point_na]
        rem_columns = np.delete(dataset.columns.values,del_dataset_col_index)
        dataset = dataset[rem_columns].copy()
        
    if node_pointer not in decision_tree:  
        decision_tree[node_pointer] = []
    feature_index = information_gain_list.index(min(information_gain_list))  #because 1st index is ID
    
    split_col = dataset.columns[feature_index+1]
    len_decision_tree = len(decision_tree) 
    if split_point_list[feature_index] is None:
        #feature is Nominal
        nominal_values = dataset[split_col].unique()
        initial_flag = True
        first_loop = True
        for x in nominal_values:
            
            #If condition to skip missing data "None"
            if x is None or x == "None":
                continue
            child_dataset = dataset[dataset[split_col] == x].copy().drop([split_col],axis=1)
            if len_decision_tree is 0 and initial_flag == True:
                decision_tree = {0: [("root")]}
                initial_flag = False

            if first_loop is True:
                is_feature_numeric_list = np.delete(is_feature_numeric_list, feature_index)
                del information_gain_list[feature_index]
                del split_point_list[feature_index]
                first_loop = False


            pos_count = np.sum(child_dataset.iloc[:,-1])
            neg_count = np.sum(~child_dataset.iloc[:,-1])
            
            if pos_count >= neg_count:
                maj_class = True
            else:
                maj_class = False
            pos_leaf = False
            neg_leaf = False
            if child_dataset.shape[1] == 2:
                if pos_count >= neg_count:
                    pos_leaf = True
                else:
                    neg_leaf = True
            if neg_count == 0 or pos_leaf == True:
                decision_tree[node_pointer].append(['nom',split_col,x,maj_class,pos_count,neg_count,"Leaf",True])
            elif pos_count == 0 or neg_leaf == True:
                decision_tree[node_pointer].append(['nom',split_col,x,maj_class,pos_count,neg_count,"Leaf",False])
            else :    
                decision_tree[node_pointer].append(['nom',split_col,x,maj_class,pos_count,neg_count,"NoLeaf",len(decision_tree.keys())])           
                recursive_node_function(information_gain_list, split_point_list,\
                    child_dataset, is_feature_numeric_list,len(decision_tree.keys()),ig)
        
               
    else:
        #feature is Continuous
        del_neg_flag = False
        del_pos_flag = True
        for j in [0,1]:
            if j == 0:
                comp = 'gt'
                child_dataset = dataset[dataset[split_col] >= split_point_list[feature_index]].copy()
            elif j == 1:
                comp = 'lt'
                child_dataset = dataset[dataset[split_col] < split_point_list[feature_index]].copy()
            pos_count = np.sum(child_dataset.iloc[:,-1])
            neg_count = np.sum(~child_dataset.iloc[:,-1])

            if pos_count >= neg_count:
                maj_class = True
            else:
                maj_class = False
        
            if neg_count == 0:
                decision_tree[node_pointer].append(['cont',comp,split_col,split_point_list[feature_index],maj_class,pos_count,neg_count,"Leaf",True])
                del_neg_flag = True
            elif pos_count == 0:
                decision_tree[node_pointer].append(['cont',comp,split_col,split_point_list[feature_index],maj_class,pos_count,neg_count,"Leaf",False])
                del_pos_flag = True
            else:
                decision_tree[node_pointer].append(['cont',comp,split_col,split_point_list[feature_index],maj_class,pos_count,neg_count,"NoLeaf",len(decision_tree.keys())])
                recursive_node_function(information_gain_list, split_point_list,\
                    child_dataset, is_feature_numeric_list,len(decision_tree.keys()),ig)     
        if del_neg_flag == True and del_pos_flag == True:
                is_feature_numeric_list = np.delete(is_feature_numeric_list, feature_index)
                del information_gain_list[feature_index]
                del split_point_list[feature_index]
     

        if node_pointer == 1:
            return decision_tree 
    #min(information_gain_list)

#recursive_node_function(information_gain_list, split_point_list,\
#    dataset,is_feature_numeric_list,1)

'''
d_t = recursive_node_function(None, None,\
    dataset,is_feature_numeric_list,1)
'''        

def tree_size(d_tree):
    size_count = 0
    #for i in range(0,len(d_tree)):
    for i in d_tree.keys():
        for j in range(0,len(d_tree[i])):
            size_count += 1
    return size_count

#def helper_max_depth(d_tree,node_pointer,m_d_c):
#    if d_tree[node_pointer][i][-2] == 'NoLeaf':
#        1+helper_max_depth(d_tree,d_tree[node_pointer][node_pointer][-1])
#    else:
#        return 1


def max_depth(d_tree,n_p,m_d_c):
    global max_depth_count
    for x in range(0,len(d_tree[n_p])):
        if d_tree[n_p][x][-2] == 'NoLeaf':
            max_depth(d_tree,d_tree[n_p][x][-1],m_d_c+1)
        else:
            if max_depth_count < m_d_c:
                max_depth_count = m_d_c
        
def prediction_function(d_t,dataset):
    pred_list = list()
    for j in range(0,dataset.shape[0]):
        n_p = 0
        break_true = False
        #for i in range(0,len(d_t[n_p])):
        for i in range(0,1):
            while True:
                if break_true == True:
                    break_true = False
                    break
                if d_t[n_p][i][0] == "cont":
                    feature = d_t[n_p][i][2]
                    comp_val = d_t[n_p][i][3]
                    data_val = dataset[feature][j]
                    if data_val >= comp_val:
                        x = 0
                    else:
                        x = 1 
                    if d_t[n_p][x][-2] == "NoLeaf": 
                        if data_val >= comp_val:
                            n_p = d_t[n_p][0][-1]
                        else:
                            n_p = d_t[n_p][1][-1]
                    else:# leaf continous
                        if data_val >= comp_val:
                            pred_list.append(d_t[n_p][0][-1])
                        else:
                            pred_list.append(d_t[n_p][1][-1]) 
                        break
                elif d_t[n_p][i][0] == "root":
                    if d_t[n_p][0][-2] == "Leaf":
                        pred_list.append(d_t[n_p][0][-1])
                        break
                    n_p = 1
                    continue
                    
                        
                else: #nominal
                    feature = d_t[n_p][0][1]
                    data_val = dataset[feature][j]
                    for ci in range (0,len(d_t[n_p])):
                        if d_t[n_p][ci][2] == data_val:
                            if d_t[n_p][ci][-2] == "NoLeaf":
                                n_p = d_t[n_p][ci][-1]
                                break 
                            else:#nominal leaf
                                pred_list.append(d_t[n_p][ci][-1])
                                break_true = True
                                break
                        if ci == len(d_t[n_p])-1:
                            #to check for incoming data which has compl new value
                            #i will not find in for loop
                            pred_list.append(d_t[n_p][ci][-1])
                            break_true = True
                            break   
                          
                
    return pred_list


def readDataType(file_name):
    is_string_numeric = list()
    with open(file_name) as f:
        for line in f:
            if ':' in line:
                d_typ = line.split(':')[1]
                d_typ = ''.join(e for e in d_typ if e.isalnum())
                if d_typ == 'continuous':
                    is_string_numeric.append(True)
                else:
                    is_string_numeric.append(False)
    return is_string_numeric[1:]



            
#pr_l = prediction_function(d_t,dataset)        
limit_max_depth = 0

new_tree = {}

def limit_max_depth_fn(d_tree,n_p,m_d_c,l_d_c):
    if n_p not in new_tree:
        new_tree[n_p] = []
    global limit_max_depth
    for x in range(0,len(d_tree[n_p])):
        new_tree[n_p].append(d_tree[n_p][x])
        if d_tree[n_p][x][-2] == 'NoLeaf':
            if l_d_c > m_d_c-1:
                new_tree[n_p][x][-2] = 'Leaf'
                new_tree[n_p][x][-1] = d_tree[n_p][x][-5]
            else:
                limit_max_depth_fn(d_tree,d_tree[n_p][x][-1],m_d_c,l_d_c+1)
    
    
def reduced_max_depth(d_tree,max_depth_given):
    global new_tree
    new_tree = {}
    limit_max_depth_fn(d_tree,0,max_depth_given,0)
    return new_tree

def get_first_split_feature(d1):
    if d1[0][0][-2] == "Leaf":
        return None
    if d1[1][0][0] == "cont": 
        return d1[1][0][2]
    elif d1[1][0][0] == "nom":
        return d1[1][0][1]
def dtree_main(path,cv,mxd,ig):

    dataset =  getData(path)
    names_f = path.split('/')[-1]
    names_file = path+'/'+names_f+'.names'
    is_feature_numeric_list = readDataType(names_file)
    mxd = int(mxd)
    
    if int(cv) == 0:
        [acc, dec_tr ] = crossValidation(dataset,is_feature_numeric_list,int(ig),mxd)

    else:
        nrow = dataset.shape[0]
        sh_lst = range(0,nrow)
        random.seed(12345)
        random.shuffle(sh_lst)
        ran_ind = int(math.floor(nrow * .70))
        train_index = sh_lst[0:ran_ind]
        test_index = sh_lst[ran_ind:nrow]

        trainData = dataset.iloc[train_index]  
        testData = dataset.iloc[test_index]   
        testData = testData.reset_index().copy()   
        dec_tr = recursive_node_function(None, None,\
            trainData,is_feature_numeric_list,0,int(ig))

        if mxd != 0:
            dec_tr = reduced_max_depth(dec_tr,mxd)
        prediction_list = prediction_function(dec_tr,testData)
        acc = accuracy_fn(prediction_list,testData[testData.columns.values[-1]])
    tree_size_nodes = tree_size(dec_tr)
    max_depth(dec_tr,0,0)
    f_feature = get_first_split_feature(dec_tr)
    acc_pr = "Accuracy: "+str(acc)
    size_pr = "Size: "+str(tree_size_nodes)
    max_depth_pr = "Maximum Depth: "+str(max_depth_count)
    f_ftr_pr = "First Feature: "+str(f_feature)
    print("Results\n{}\n{}\n{}\n{}\n".format(acc_pr,size_pr,max_depth_pr,f_ftr_pr))
    print("The End") 

if __name__ == "__main__":
  
#1. path to the file
#2.cross-validation =0, ncv = 1
#3.max_depth_of_tree
#4.information gain 0 , gain ration 1
#dtree_main(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    dtree_main(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])




