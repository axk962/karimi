import numpy as np
from mldata import *
import math
import random

class DataAcq(object):
    def get_data(self, path):
        # ml_dataset = parse_c45('example')
        file_name = path.split("/")[-1]
        file_path = '/'.join(path.split("/")[0:-1]) + '/.'
        ml_dataset = parse_c45(file_name, file_path)
        dataset_df = np.array(ml_dataset)
        return dataset_df

    def read_data_type(self, file_name):
        is_string_numeric = list()
        with open(file_name) as f:
            for line in f:
                if ':' in line:
                    d_typ = line.split(':')[1]
                    d_typ = ''.join(e for e in d_typ if e.isalnum())
                    if d_typ == 'continuous':
                        is_string_numeric.append(True)
                    else:
                        is_string_numeric.append(False)
        return is_string_numeric[1:]


def accuracy(prd, act):
    if len(prd) != len(act):
        print("accuracy wrong")
        return
    TP = 0
    TN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 0:
            TN = TN + 1
    return (TP + TN) * 1.0 / len(prd)

def precision(prd, act):
    if len(prd) != len(act):
        print("precision wrong")
        return
    TP = 0
    FP = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 1 and act[i] == 0:
            FP = FP + 1
    if TP + FP == 0:
        return float('nan')
    return TP * 1.0 / (TP + FP)

def recall(prd, act):
    if len(prd) != len(act):
        print("recall wrong")
        return
    TP = 0
    FN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 1:
            FN = FN + 1
    if TP + FN == 0:
        return float('nan')
    return TP * 1.0 / (TP + FN)

def auc(prd_n, prd_tf, act):
    if len(prd_n) != len(prd_tf) and len(prd_tf) != len(act):
        print("length wrong")
        return
    m = np.zeros(shape=[len(prd_n)],
                 dtype=[('prd_n', float), ('prd_tf', float), ('act', float), ('tpr', float), ('fpr', float)])
    for i in range(0, len(prd_n)):
        m[i][0] = prd_n[i]
        m[i][1] = prd_tf[i]
        m[i][2] = act[i]
    # print (m)
    m[::-1].sort(order='prd_n')
    # print(m)
    TP = 0
    allPos = 0
    FP = 0
    allNeg = 0
    for i in range(0, len(act)):
        if m[i][2] == 1:
            allPos = allPos + 1
        elif m[i][2] == 0:
            allNeg = allNeg + 1

    for i in range(0, len(prd_tf)):
        # Calculating TPR and FPR
        if m[i][2] == 1:
            TP += 1
        else:
            FP += 1
        m[i][3] = TP * 1.0 / allPos
        m[i][4] = FP * 1.0 / allNeg

    aroc = 0
    for i in range(0, len(prd_tf) - 1):
        aroc = aroc + 0.5 * (m[i + 1][3] + m[i][3]) * (m[i + 1][4] - m[i][4])

    return aroc

def crossValidation(data_array):
    pos = []
    neg = []
    kfold_train_ar = []
    kfold_test_ar = []
    for i in range(0, data_array.shape[0]):
        if data_array[i][-1] == 'True':
            pos.append(i)
        else:
            neg.append(i)
    #print(pos)
    #print(neg)

    p = math.floor(0.2 * data_array.shape[0])
    temp = float(len(pos)) / data_array.shape[0] * p
    if (temp - int(temp) >= 0.5):
        ppn = math.ceil(float(len(pos)) / data_array.shape[0] * p)
    else:
        ppn = math.floor(float(len(pos)) / data_array.shape[0] * p)

    random.seed(12345)
    random.shuffle(pos)
    random.shuffle(neg)

    newArray = []

    c = 0
    while c < 5:
        cPos = ppn
        cNeg = p - ppn
        if c < 4:
            while cPos >= 1:
                newArray.append(data_array[pos[0]])
                tmp = pos[0]
                pos.remove(tmp)
                cPos -= 1
            while cNeg >= 1:
                newArray.append(data_array[neg[0]])
                tmp = neg[0]
                neg.remove(tmp)
                cNeg -= 1
        elif c == 4:
            while len(pos) > 0:
                newArray.append(data_array[pos[0]])
                tmp = pos[0]
                pos.remove(tmp)
            while len(neg) > 0:
                newArray.append(data_array[neg[0]])
                tmp = neg[0]
                neg.remove(tmp)
        c += 1
    npNewArray = np.array(newArray)
    # print (npNewArray)
    for i in range(0, npNewArray.shape[0], int(p)):
        tempTr = []
        tempTs = []
        if i == 0:
            ite = i
            for j in range(0, int(p)):
                tempTs.append(npNewArray[ite])
                ite += 1
            for k in range(0, (npNewArray.shape[0] - int(p))):
                tempTr.append(npNewArray[k + ite])
        elif i == (npNewArray.shape[0] - int(p)):
            for j in range(0, int(p)):
                tempTs.append(npNewArray[j + i])
            for k in range(0, (npNewArray.shape[0] - int(p))):
                tempTr.append(npNewArray[k])
        elif (i + int(p)) > npNewArray.shape[0]:
            break
        else:
            ite = i
            for j in range(0, int(p)):
                tempTs.append(npNewArray[ite])
                ite += 1
            for k in range(0, i):
                tempTr.append(npNewArray[k])
            for m in range(ite, npNewArray.shape[0]):
                tempTr.append(npNewArray[m])

        train = np.array(tempTr)
        test = np.array(tempTs)
        kfold_train_ar.append(train)
        kfold_test_ar.append(test)
        # print(train)
        # print(test)
    return kfold_train_ar, kfold_test_ar

def fit_nbayes (data, m):
    Y = data[:, -1]
    n_Y_Pos = 0.0
    n_Y_Neg = 0.0
    for i in range(0, len(Y)):
        if Y[i] == 'True':
            n_Y_Pos += 1.0
        elif Y[i] == 'False':
            n_Y_Neg += 1.0
    pos = n_Y_Pos / len(Y)
    neg = n_Y_Neg / len(Y)
    #print (n_Y_Pos)
    #print (n_Y_Neg)
    notes =[]
    for i in range(0, data.shape[1]-1):
        temp = np.unique(data[:, i])
        new_probs = np.zeros((len(temp), 3))
        for j in range(0, len(temp)):
            new_probs[j, 0] = temp[j]
            sub_Y_Pos = 0
            sub_Y_Neg = 0
            for k in range(0, data.shape[0]):
                if temp[j] == data[k, i] and Y[k] == 'True':
                    sub_Y_Pos += 1
                elif temp[j] == data[k, i] and Y[k] == 'False':
                    sub_Y_Neg += 1
            p = 1.0/len(temp)
            if m < 0:
                m = len(temp)
            new_probs[j, 1] = (sub_Y_Pos + m*p)/(n_Y_Pos + m)
            new_probs[j, 2] = (sub_Y_Neg + m*p)/(n_Y_Neg + m)

        notes.append(new_probs)
    return [pos, neg, notes]

def predict(data, notes, pos, neg):
    t = np.log(pos)
    f = np.log(neg)

    for i in range(0, len(data)-1):
        temp = np.array(notes[i][:, 0])
        temp = temp.astype(int)
        for j in range(0, len(temp)):
            #print(data[i])
            #print(temp[j])
            if data[i] == temp[j]:
                t += np.log(notes[i][j, 1])
                f += np.log(notes[i][j, 2])
    t = np.exp(t)
    f = np.exp(f)
    return t, f

def main_nbayes(path, cv, num_bins, m_est):
    names_f = path.split('/')[-1]
    names_file = path + '/' + names_f + '.names'

    dacq = DataAcq()

    dataset = dacq.get_data(path)
    is_cont = dacq.read_data_type(names_file)
    for j in xrange(len(is_cont)):
        if is_cont[j]:
            temp = dataset[:,j+1]
            temp = temp.astype(float)
            max = np.amax(temp)
            min = np.amin(temp)

            temp = temp - min
            temp = temp / (max - min)*num_bins
            temp = np.floor(temp)
            temp[temp < 0] = 0
            temp[temp >= num_bins] = num_bins - 1
            dataset[:, j+1] = temp

            #print(dataset[:,1])
        else:
            nom_values = np.unique(dataset[:, j+1])
            for i in xrange(len(nom_values)):
                dataset[:, j+1][dataset[:, j+1] == nom_values[i]] = i + 1.0

    ncol = dataset.shape[1]
    #Y = dataset[:, -1]
    dataset = np.delete(dataset,0,1)
    if cv == 0:
        training_list, test_list = crossValidation(dataset)
        cv_itter = 5
        acc_arr = []
        prec_arr = []
        rec_arr = []
        prd_n_arr = []
        prd_TF_arr = []
        act_arr = []
    elif cv == 1:
        cv_itter = 1

    for counter in range(0, cv_itter):
        if cv == 0:
            [pos, neg, dic] = fit_nbayes(training_list[counter], m_est)
            prd = []
            for i in range(0, test_list[counter].shape[0]):
                if test_list[counter][i, -1] == 'True':
                    test_list[counter][i, -1] = 1
                elif test_list[counter][i, -1] == 'False':
                    test_list[counter][i, -1] = 0
            test_list[counter] = test_list[counter].astype(float)
            prd_n = []
            for i in range(0, test_list[counter].shape[0]):
                [t, f] = predict(test_list[counter][i, :], dic, pos, neg)
                if t >= f:
                    prd_n.append(t)
                    t = 1
                elif t < f:
                    prd_n.append(f)
                    t = 0
                prd.append(t)
            Y = test_list[counter][:, -1]
            acc = accuracy(prd, Y)
            rc = recall(prd, Y)
            prc = precision(prd, Y)
            acc_arr.append(acc)
            rec_arr.append(rc)
            prec_arr.append(prc)

            prd_TF_arr.append(prd)
            prd_n_arr.append(prd_n)
            act_arr.append(Y)
        elif cv == 1:
            [pos, neg, dic] = fit_nbayes(dataset, m_est)
            prd = []
            for i in range(0, dataset.shape[0]):
                if dataset[i, -1] == 'True':
                    dataset[i, -1] = 1
                elif dataset[i, -1] == 'False':
                    dataset[i, -1] = 0
            dataset = dataset.astype(float)
            prd_n = []
            for i in range(0, dataset.shape[0]):
                [t, f] = predict(dataset[i, :], dic, pos, neg)
                if t >= f:
                    prd_n.append(t)
                    t = 1
                elif t < f:
                    prd_n.append(f)
                    t = 0
                prd.append(t)
            Y = dataset[:, -1]
            acc = accuracy(prd, Y)
            rc = recall(prd, Y)
            prc = precision(prd, Y)

            print ("For Full dataset with no value for Standard Deviations")
            print ("Accuracy: {} NA".format(acc))
            print ("Precision: {} NA".format(prc))
            print ("Recall: {} NA".format(rc))
            auc_val = auc(prd_n, prd, Y)
            print ("Area Under ROC: {} ".format(auc_val))


    if cv == 0:
        print ("Accuracy: {} {} ".format(np.mean(acc_arr), np.std(acc_arr)))
        print ("Precision: {} {} ".format(np.mean(prec_arr), np.std(prec_arr)))
        print ("Recall: {} {} ".format(np.mean(rec_arr), np.std(rec_arr)))

        prd_n_arr = [item for sublist in prd_n_arr for item in sublist]
        prd_TF_arr = [item for sublist in prd_TF_arr for item in sublist]
        act_arr = [item for sublist in act_arr for item in sublist]

        auc_val = auc(prd_n_arr, prd_TF_arr, act_arr)
        print ("Area Under ROC: {} ".format(auc_val))


    print("done")



if __name__ == "__main__":
    #1. the path to the data
    #2. 0/1 option. If 0, use cross validation. If 1, run the algorithm on the full sample.
    #3. number of bins for any continuous feature (at least 2).
    #4. the value of m for the m-estimate. If this value is negative, use Laplace smoothing.
    #main_fuc("/Users/Chen/Documents/eecs440_fall2017_karimi_zhao/P3/440data/voting", 0, 2, -3)
    main_nbayes(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]))