import numpy as np
import math
import random
from dtree import *
from ann import *
from nbayes import *
from logreg import *
from mldata import *


class DataAcq(object):
    def get_data(self, path):
        # ml_dataset = parse_c45('example')
        file_name = path.split("/")[-1]
        file_path = '/'.join(path.split("/")[0:-1]) + '/.'
        ml_dataset = parse_c45(file_name, file_path)
        dataset_df = np.array(ml_dataset)
        return dataset_df

    def read_data_type(self, file_name):
        is_string_numeric = list()
        with open(file_name) as f:
            for line in f:
                if ':' in line:
                    d_typ = line.split(':')[1]
                    d_typ = ''.join(e for e in d_typ if e.isalnum())
                    if d_typ == 'continuous':
                        is_string_numeric.append(True)
                    else:
                        is_string_numeric.append(False)
        return is_string_numeric[1:]


def accuracy(prd, act):
    if len(prd) != len(act):
        print("accuracy wrong")
        return
    TP = 0
    TN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 0:
            TN = TN + 1
    return (TP + TN) * 1.0 / len(prd)

def precision(prd, act):
    if len(prd) != len(act):
        print("precision wrong")
        return
    TP = 0
    FP = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 1 and act[i] == 0:
            FP = FP + 1
    if TP + FP == 0:
        return float('nan')
    return TP * 1.0 / (TP + FP)

def recall(prd, act):
    if len(prd) != len(act):
        print("recall wrong")
        return
    TP = 0
    FN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 1:
            FN = FN + 1
    if TP + FN == 0:
        return float('nan')
    return TP * 1.0 / (TP + FN)

def auc(prd_n, prd_tf, act):
    if len(prd_n) != len(prd_tf) and len(prd_tf) != len(act):
        print("length wrong")
        return
    m = np.zeros(shape=[len(prd_n)],
                 dtype=[('prd_n', float), ('prd_tf', float), ('act', float), ('tpr', float), ('fpr', float)])
    for i in range(0, len(prd_n)):
        m[i][0] = prd_n[i]
        m[i][1] = prd_tf[i]
        m[i][2] = act[i]
    # print (m)
    m[::-1].sort(order='prd_n')
    # print(m)
    TP = 0
    allPos = 0
    FP = 0
    allNeg = 0
    for i in range(0, len(act)):
        if m[i][2] == 1:
            allPos = allPos + 1
        elif m[i][2] == 0:
            allNeg = allNeg + 1

    for i in range(0, len(prd_tf)):
        # Calculating TPR and FPR
        if m[i][2] == 1:
            TP += 1
        else:
            FP += 1
        m[i][3] = TP * 1.0 / allPos
        m[i][4] = FP * 1.0 / allNeg

    aroc = 0
    for i in range(0, len(prd_tf) - 1):
        aroc = aroc + 0.5 * (m[i + 1][3] + m[i][3]) * (m[i + 1][4] - m[i][4])

    return aroc

def crossValidation(data_array):
    pos = []
    neg = []
    kfold_train_ar = []
    kfold_test_ar = []
    for i in range(0, data_array.shape[0]):
        if data_array[i][-1] == 'True':
            pos.append(i)
        else:
            neg.append(i)
    #print(pos)
    #print(neg)

    p = math.floor(0.2 * data_array.shape[0])
    temp = float(len(pos)) / data_array.shape[0] * p
    if (temp - int(temp) >= 0.5):
        ppn = math.ceil(float(len(pos)) / data_array.shape[0] * p)
    else:
        ppn = math.floor(float(len(pos)) / data_array.shape[0] * p)

    random.seed(12345)
    random.shuffle(pos)
    random.shuffle(neg)

    newArray = []

    c = 0
    while c < 5:
        cPos = ppn
        cNeg = p - ppn
        if c < 4:
            while cPos >= 1:
                newArray.append(data_array[pos[0]])
                tmp = pos[0]
                pos.remove(tmp)
                cPos -= 1
            while cNeg >= 1:
                newArray.append(data_array[neg[0]])
                tmp = neg[0]
                neg.remove(tmp)
                cNeg -= 1
        elif c == 4:
            while len(pos) > 0:
                newArray.append(data_array[pos[0]])
                tmp = pos[0]
                pos.remove(tmp)
            while len(neg) > 0:
                newArray.append(data_array[neg[0]])
                tmp = neg[0]
                neg.remove(tmp)
        c += 1
    npNewArray = np.array(newArray)
    # print (npNewArray)
    for i in range(0, npNewArray.shape[0], int(p)):
        tempTr = []
        tempTs = []
        if i == 0:
            ite = i
            for j in range(0, int(p)):
                tempTs.append(npNewArray[ite])
                ite += 1
            for k in range(0, (npNewArray.shape[0] - int(p))):
                tempTr.append(npNewArray[k + ite])
        elif i == (npNewArray.shape[0] - int(p)):
            for j in range(0, int(p)):
                tempTs.append(npNewArray[j + i])
            for k in range(0, (npNewArray.shape[0] - int(p))):
                tempTr.append(npNewArray[k])
        elif (i + int(p)) > npNewArray.shape[0]:
            break
        else:
            ite = i
            for j in range(0, int(p)):
                tempTs.append(npNewArray[ite])
                ite += 1
            for k in range(0, i):
                tempTr.append(npNewArray[k])
            for m in range(ite, npNewArray.shape[0]):
                tempTr.append(npNewArray[m])

        train = np.array(tempTr)
        test = np.array(tempTs)
        kfold_train_ar.append(train)
        kfold_test_ar.append(test)
        # print(train)
        # print(test)
    return kfold_train_ar, kfold_test_ar

def main_fuc(path, cv, learn_alg, itr):
    names_f = path.split('/')[-1]
    names_file = path + '/' + names_f + '.names'

    dacq = DataAcq()

    dataset = dacq.get_data(path)

    dataset = np.delete(dataset, 0, 1)
    if cv == 0:
        training_list, test_list = crossValidation(dataset)
        weight_lists = []
        for i in range(0, 5):
            N = training_list[i].shape[0]
            weights = np.zeros(N)
            weights[:] = 1 / N
            weight_lists.append(weights)
        cv_itter = 5
        weight_note_0 = []
        weight_note_1 = []
        weight_note_2 = []
        weight_note_3 = []
        weight_note_4 = []
        v_w_note_0 = []
        v_w_note_1 = []
        v_w_note_2 = []
        v_w_note_3 = []
        v_w_note_4 = []
        acc_arr = []
        prec_arr = []
        rec_arr = []
        prd_n_arr = []
        prd_TF_arr = []
        act_arr = []
    elif cv == 1:
        cv_itter = 1
        N = dataset.shape[0]
        weights = np.zeros(N)
        weights[:] = 1 / N
        weight_note = []
        v_w_note = []

    if cv == 0:
        for i in range(0, 5):
            train = training_list[i]
            class_weights_list = []
            count = 0
            for j in range(0, itr):
                if learn_alg == 'nbayes':
                    [Y_hat, Y, class_weight] = main_nbayes(train, weight_lists[i])
                elif learn_alg == 'dtree':
                    [Y_hat, Y, class_weight] = dtree_main(train, weight_lists[i])
                elif learn_alg == 'ann':
                    [Y_hat, Y, class_weight] = main_ann(train, weight_lists[i])
                elif learn_alg == 'logreg':
                    [Y_hat, Y, class_weight] = main_logreg(train, weight_lists[i])

                epsilon = weight_lists[i][Y_hat != Y].sum()
                alpha = 0.5 * np.log((1 - epsilon) / epsilon)

                if i == 0:
                    weight_note_0.append(weight_lists[i])
                    v_w_note_0.append(alpha)
                elif i == 1:
                    weight_note_1.append(weight_lists[i])
                    v_w_note_1.append(alpha)
                elif i == 2:
                    weight_note_2.append(weight_lists[i])
                    v_w_note_2.append(alpha)
                elif i == 3:
                    weight_note_3.append(weight_lists[i])
                    v_w_note_3.append(alpha)
                elif i == 4:
                    weight_note_4.append(weight_lists[i])
                    v_w_note_4.append(alpha)

                class_weights_list.append(class_weight)
                count += 1
                if epsilon == 0 or epsilon > 0.5:
                    break

                new_weights = weight_lists[i] * np.exp(-alpha * Y * Y_hat)
                weight_lists[i] = new_weights / new_weights.sum()

            if i == 0:
                temp_weights = weight_note_0
                temp_v_w = v_w_note_0
            elif i == 1:
                temp_weights = weight_note_1
                temp_v_w = v_w_note_1
            elif i == 2:
                temp_weights = weight_note_2
                temp_v_w = v_w_note_2
            elif i == 3:
                temp_weights = weight_note_3
                temp_v_w = v_w_note_3
            elif i == 4:
                temp_weights = weight_note_4
                temp_v_w = v_w_note_4
            v_w_sum = sum(temp_v_w)
            test = test_list[i]
            Y_test = test[:, -1]
            final = np.zeros(len(temp_v_w))
            for k in range(0, count):
                if learn_alg == 'nbayes':
                    Y_hat = nbayes_prd(test, temp_weights[k], class_weights_list[k])
                elif learn_alg == 'dtree':
                    Y_hat = dtree_prd(test, temp_weights[k], class_weights_list[k])
                elif learn_alg == 'ann':
                    Y_hat = ann_prd(test, temp_weights[k], class_weights_list[k])
                elif learn_alg == 'logreg':
                    Y_hat = logreg_prd(test, temp_weights[k], class_weights_list[k])

                final += (temp_v_w[k]/v_w_sum)*Y_hat
            final = final/2 + 0.5
            final[final >= 0.5] = 1
            final[final != 1] = -1

            acc = accuracy(final, Y_test)

    elif cv == 1:
        class_weights_list = []
        count = 0
        for j in range(0, itr):
            if learn_alg == 'nbayes':
                [Y_hat, Y, class_weight] = main_nbayes(dataset, weights)
            elif learn_alg == 'dtree':
                [Y_hat, Y, class_weight] = dtree_main(dataset, weights)
            elif learn_alg == 'ann':
                [Y_hat, Y, class_weight] = main_ann(dataset, weights)
            elif learn_alg == 'logreg':
                [Y_hat, Y, class_weight] = main_logreg(dataset, weights)

            epsilon = weights[Y_hat != Y].sum()
            alpha = 0.5 * np.log((1 - epsilon) / epsilon)

            weight_note.append(weights)
            v_w_note.append(alpha)

            class_weights_list.append(class_weight)
            count += 1
            if epsilon == 0 or epsilon > 0.5:
                break

            new_weights = weights * np.exp(-alpha * Y * Y_hat)
            weights = new_weights / new_weights.sum()

        v_w_sum = sum(v_w_note)
        Y_test = dataset[:, -1]
        final = np.zeros(len(v_w_note))
        for k in range(0, count):
            if learn_alg == 'nbayes':
                Y_hat = nbayes_prd(dataset, weight_note[k], class_weights_list[k])
            elif learn_alg == 'dtree':
                Y_hat = dtree_prd(dataset, weight_note[k], class_weights_list[k])
            elif learn_alg == 'ann':
                Y_hat = ann_prd(dataset, weight_note[k], class_weights_list[k])
            elif learn_alg == 'logreg':
                Y_hat = logreg_prd(dataset, weight_note[k], class_weights_list[k])

            final += (v_w_note[k] / v_w_sum) * Y_hat
        final = final / 2 + 0.5
        final[final >= 0.5] = 1
        final[final != 1] = -1

        acc = accuracy(final, Y_test)

    #if cv == 0:
    #    print(acc)
    #elif cv == 1:
    #    print(acc)

if __name__ == "__main__":
    #1. the path to the data
    #2. 0/1 option. If 0, use cross validation. If 1, run the algorithm on the full sample.
    #3. learning algorithm: dtree, ann, nbayes or logreg
    #4. iteration boost going to run
    main_fuc("/Users/Chen/Documents/eecs440_fall2017_karimi_zhao/P3/440data/voting", 0, 'nbayes', 100)
    #main_fuc(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]))