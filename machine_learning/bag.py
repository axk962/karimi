import numpy as np
import numpy as np
import math
import random
from mldata import *
from cross_validation import *

from bag_ann import main_ann
from bag_logreg import main_logreg
from bag_nbayes import main_nbayes
from bag_dtree import dtree_main
import pdb


class DataAcq(object):
    def get_data(self, path):
        # ml_dataset = parse_c45('example')
        file_name = path.split("/")[-1]
        file_path = '/'.join(path.split("/")[0:-1]) + '/.'
        ml_dataset = parse_c45(file_name, file_path)
        dataset_df = np.array(ml_dataset)
        return dataset_df

    def read_data_type(self, file_name):
        is_string_numeric = list()
        with open(file_name) as f:
            for line in f:
                if ':' in line:
                    d_typ = line.split(':')[1]
                    d_typ = ''.join(e for e in d_typ if e.isalnum())
                    if d_typ == 'continuous':
                        is_string_numeric.append(True)
                    else:
                        is_string_numeric.append(False)
        return is_string_numeric[1:]

#bagging of the dataset
def subsample(dataset, ratio=1.0):
    sample = list()
    n_sample = round(len(dataset) * ratio)
    while len(sample) < n_sample:
        index = random.randrange(len(dataset))
        sample.append(dataset[index])
    sample = np.array(sample)
    return sample

def accuracy(prd, act):
    if len(prd) != len(act):
        print("accuracy wrong")
        return
    TP = 0
    TN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 0:
            TN = TN + 1
    return (TP + TN) * 1.0 / len(prd)

def precision(prd, act):
    if len(prd) != len(act):
        print("precision wrong")
        return
    TP = 0
    FP = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 1 and act[i] == 0:
            FP = FP + 1
    if TP + FP == 0:
        return float('nan')
    return TP * 1.0 / (TP + FP)

def recall(prd, act):
    if len(prd) != len(act):
        print("recall wrong")
        return
    TP = 0
    FN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 1:
            FN = FN + 1
    if TP + FN == 0:
        return float('nan')
    return TP * 1.0 / (TP + FN)

def auc(prd_n, prd_tf, act):
    if len(prd_n) != len(prd_tf) and len(prd_tf) != len(act):
        print("length wrong")
        return
    m = np.zeros(shape=[len(prd_n)],
                 dtype=[('prd_n', float), ('prd_tf', float), ('act', float), ('tpr', float), ('fpr', float)])
    for i in range(0, len(prd_n)):
        m[i][0] = prd_n[i]
        m[i][1] = prd_tf[i]
        m[i][2] = act[i]
    # print (m)
    m[::-1].sort(order='prd_n')
    # print(m)
    TP = 0
    allPos = 0
    FP = 0
    allNeg = 0
    for i in range(0, len(act)):
        if m[i][2] == 1:
            allPos = allPos + 1
        elif m[i][2] == 0:
            allNeg = allNeg + 1

    for i in range(0, len(prd_tf)):
        # Calculating TPR and FPR
        if m[i][2] == 1:
            TP += 1
        else:
            FP += 1
        m[i][3] = TP * 1.0 / allPos
        m[i][4] = FP * 1.0 / allNeg

    aroc = 0
    for i in range(0, len(prd_tf) - 1):
        aroc = aroc + 0.5 * (m[i + 1][3] + m[i][3]) * (m[i + 1][4] - m[i][4])

    return aroc

def main_bag(path, cv, learn_alg, itr):
    if learn_alg == "dtree":
        #1. path to the file
        #2.cross-validation =0, ncv = 1
        #3.max_depth_of_tree
        #4.information gain 0 , gain ration 1
        #5. no. of bagging itteration
        #dtree_main(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
        dtree_main(path,cv,1,0,itr)
    elif learn_alg == "ann":
        #1. path to the file
        #2. cross-validation
        #3. hidden Unit
        #4. decay Coefficient
        #5. Ann iteration
        #6. Bagging Itteration
        main_ann(path,cv,0,.0001,0,itr)
    elif learn_alg == "logreg":
        #1. path to the file
        #2. cross validation
        #3. lambda const
        #4. bagging itteration
        main_logreg(path, cv, 1,itr)
        print("Log Reg")
    elif learn_alg == "nbayes":
        #1. the path to the data
        #2. 0/1 option. If 0, use cross validation. If 1, run the algorithm on the full sample.
        #3. number of bins for any continuous feature (at least 2).
        #4. the value of m for the m-estimate. If this value is negative, use Laplace smoothing.
        #main_fuc("/Users/Chen/Documents/eecs440_fall2017_karimi_zhao/P3/440data/voting", 0, 2, -3)
        main_nbayes(path, int(cv), 5, 2,itr)

        print("N Bayes")
    else:
        print("Please enter the correct learning algorithm\n")


#    names_f = path.split('/')[-1]
#    names_file = path + '/' + names_f + '.names'
#    dacq = DataAcq()
#    dataset = dacq.get_data(path)
#    dataset = np.delete(dataset, 0, 1)
#    is_cont = dacq.read_data_type(names_file) 
#    #dataset = subsample(dataset)
#    if cv == 0:
#        training_list, test_list = crossValidation(dataset)
#        weight_lists = []
#        for i in range(0, 5):
#            N = training_list[i].shape[0]
#            weights = np.zeros(N)
#            weights[:] = 1 / N
#            weight_lists.append(weights)
#        cv_itter = 5

if __name__ == "__main__":
    pdb.set_trace()
    #1. the path to the data
    #2. 0/1 option. If 0, use cross validation. If 1, run the algorithm on the full sample.
    #3. learning algorithm: dtree, ann, nbayes or logreg
    #4. iteration bag going to run
    main_bag(sys.argv[1], int(sys.argv[2]), sys.argv[3], int(sys.argv[4]))
