import numpy as np
import sys
import operator
import math
import random

def err(Y, yHat):
    return sum(np.sqrt((Y - yHat)** 2))*1.0/len(Y)

def accuracy(prd, act):
    if len(prd) != len(act):
        print("accuracy wrong")
        return
    TP = 0
    TN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 0:
            TN = TN + 1
    return (TP + TN)*1.0/len(prd)

def precision(prd, act):
    if len(prd) != len(act):
        print("precision wrong")
        return
    TP = 0
    FP = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 1 and act[i] == 0:
            FP = FP + 1
    if TP+FP == 0:
        return float('nan')
    return TP*1.0/(TP+FP)

def recall(prd, act):
    if len(prd) != len(act):
        print("recall wrong")
        return
    TP = 0
    FN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 1:
            FN = FN + 1
    if TP+FN == 0:
        return float('nan')
    return TP*1.0/(TP+FN)

def convertN2TF(prd):
    """
    Convert real number between range of [ 0 , 1] into True or False in an Array
    """
    a = []
    for i in range(0, len(prd)):
        if prd[i] > 0.5:
            a.append(1)
        elif prd[i] <= 0.5:
            a.append(0)
    return a

def auc(prd_n, prd_tf, act):
    """
    calculate the area under the curve based on arrays of predicted values,
    arrays with True False class and array having actual True or False 
    annotation
    """
    if len(prd_n) != len(prd_tf) and len(prd_tf) != len(act):
        print("length wrong")
        return
    m = np.zeros(shape=[len(prd_n)], dtype=[('prd_n', float), ('prd_tf', float)\
                        , ('act', float), ('tpr', float), ('fpr', float)])
    for i in range(0, len(prd_n)):
        m[i][0] = prd_n[i]
        m[i][1] = prd_tf[i]
        m[i][2] = act[i]
    #print (m)
    m[::-1].sort(order='prd_n')
    #print(m)
    TP = 0
    allPos = 0
    FP = 0
    allNeg = 0
    for i in range(0, len(act)):
        if m[i][2] == 1:
            allPos = allPos + 1
        elif m[i][2] == 0:
            allNeg = allNeg + 1

    for i in range(0,len(prd_tf)):
        #Calculating TPR and FPR
        if m[i][2] == 1:
            TP += 1
        else :
            FP += 1
        m[i][3] = TP*1.0/allPos
        m[i][4] = FP*1.0/allNeg
            

    aroc = 0
    for i in range(0, len(prd_tf)-1):
        aroc = aroc + 0.5 * (m[i+1][3] + m[i][3]) * (m[i+1][4] - m[i][4])

    return aroc

def crossValidation(data_array):
    """
    Implementation of 5-fold stratified partion of dataset into K training and 
    K test sets.
    """
    pos = []
    neg = []
    kfold_train_ar = []
    kfold_test_ar = []
    for i in range(0, data_array.shape[0]):
        if data_array[i][-1] == True:
            pos.append(i)
        else:
            neg.append(i)
    p = math.floor(0.2 * data_array.shape[0])
    temp = float(len(pos)) / data_array.shape[0] * p
    if (temp - int(temp) >= 0.5):
        ppn = math.ceil(float(len(pos)) / data_array.shape[0] * p)
    else:
        ppn = math.floor(float(len(pos)) / data_array.shape[0] * p)

    #random.seed(12345)
    random.shuffle(pos)
    random.shuffle(neg)
    newArray = []
    c = 0
    while c < 5:
        cPos = ppn
        cNeg = p - ppn
        if c < 4:
            while cPos >= 1:
                newArray.append(data_array[pos[0]])
                tmp = pos[0]
                pos.remove(tmp)
                cPos -= 1
            while cNeg >= 1:
                newArray.append(data_array[neg[0]])
                tmp = neg[0]
                neg.remove(tmp)
                cNeg -= 1
        elif c == 4:
            while len(pos) > 0:
                newArray.append(data_array[pos[0]])
                tmp = pos[0]
                pos.remove(tmp)
            while len(neg) > 0:
                newArray.append(data_array[neg[0]])
                tmp = neg[0]
                neg.remove(tmp)
        c += 1
    npNewArray = np.array(newArray)
    #print (npNewArray)
    for i in range(0, npNewArray.shape[0], int(p)):
        tempTr = []
        tempTs = []
        if i == 0:
            ite = i
            for j in range(0, int(p)):
                tempTs.append(npNewArray[ite])
                ite += 1
            for k in range(0, (npNewArray.shape[0] - int(p))):
                tempTr.append(npNewArray[k + ite])
        elif i == (npNewArray.shape[0] - int(p)):
            for j in range(0, int(p)):
                tempTs.append(npNewArray[j + i])
            for k in range(0, (npNewArray.shape[0] - int(p))):
                tempTr.append(npNewArray[k])
        elif (i+int(p)) > npNewArray.shape[0]:
            break
        else:
            ite = i
            for j in range(0, int(p)):
                tempTs.append(npNewArray[ite])
                ite += 1
            for k in range(0, i):
                tempTr.append(npNewArray[k])
            for m in range(ite, npNewArray.shape[0]):
                tempTr.append(npNewArray[m])
        
        train = np.array(tempTr)
        test = np.array(tempTs)
        kfold_train_ar.append(train)
        kfold_test_ar.append(test)
        #print(train)
        #print(test)
    return kfold_train_ar, kfold_test_ar


def gradientHidden(input, output_hat, dL_next, W_next):
    """
    Calculates the gradient values of hidden layer of neural network
    """
    k = len(input)
    n = 1
    m = len(output_hat)
    p = len(W_next)

    downstream = (W_next.reshape(1, p) *
                  dL_next).sum()

    dl = (1 - output_hat).reshape(1, m) * downstream
    dl = dl.reshape(n, m) * input.reshape(k, n)
    return dl

def gradient_output(input, output, output_hat):
    """
    Calculates the gradient values of the outputlayer of neural network
    """
    k = len(input)
    dl = output_hat * (1 - output_hat) * (output_hat - output)
    dl = dl * input.reshape(k, 1)

    return dl

def back(input, hidden_input, output, output_hat, weightToHidden,\
         weightToOutput, eta, gamma):
    """
    Backpropagation function of neural network, it helps converge the neural 
    network when the changes in the weight become small or negligible or in
    other words weight become stable.
    Args:
      input (array): first layer input
      hidden_input (array): the input to the output layer
      output (array): actual output
      output_hat (array): predicted output
      weightToHidden (array): weight from input layer onto hidden layer
      weightToOutput (array): weight from hidden layer onto output layer
      eta (float): learning rate
      gamma (float): weight decay
    """
    dl = gradient_output(hidden_input, output, output_hat)
    dl_hidden = gradientHidden(input, hidden_input, dl, weightToOutput)
    newWeightToHidden = weightToHidden - eta * dl_hidden - eta * gamma * weightToHidden
    newWeightToOutput = weightToOutput - eta * dl -eta * gamma * weightToOutput
    return newWeightToHidden, newWeightToOutput

def backTwoLayer(inputs, outputs, outputs_hat, weight, eta, gamma):
    """
    Backpropagation function of neural network, it helps converge the neural 
    network when the changes in the weight become small or negligible or in
    other words weight become stable.
    Args:
      inputs (array): first layer input
      outputs (array): actual output
      outputs_hat (array): predicted output
      weight (array): weight from input layer onto hidden layer
      eta (float): learning rate
      gamma (float): weight decay
    """
    k = len(inputs)
    dl = outputs_hat * (1 - outputs_hat) * (outputs_hat - outputs)
    dl = dl* inputs.reshape(k, 1)
    newWeight = weight - eta * dl - eta * gamma * weight
    
    return newWeight
