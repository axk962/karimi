import numpy as np
import pdb
from mldata import *
from action import *
import operator
import math
import random
from bag_data import bagsample
from scipy import stats

class Neural_Network(object):
    def __init__(self,size_input,hidden_unit,output_size=1):
        self.size_input_l = size_input
        self.size_hidden_l = hidden_unit+1 #for bias neuron
        self.size_output_l = output_size

        if hidden_unit == 0:#2layer ANN
            self.size_hidden_l = output_size

        self.W1 = np.random.uniform(low=-0.1, high=0.1, size=(self.size_input_l*self.size_hidden_l,))
        self.W1.shape = (self.size_input_l,self.size_hidden_l)
        self.W2 = np.random.uniform(low=-0.1, high=0.1, size=((self.size_output_l*self.size_hidden_l),))
        self.W2.shape = (self.size_hidden_l, self.size_output_l)

    def updateWeight(self, newW1, newW2=0.0):
        self.W1 = newW1
        self.W2 = newW2

    def feedForward(self, data_m,i):
        self.i2 = np.dot(data_m[i, :],self.W1)
        self.o2 = self.sigmoid(self.i2)
        #update the last column as -1, because input from bias=-1
        ncol_o2 = self.o2.shape[0] -1
        #output from bias hidden node should be -1
        self.o2[ncol_o2] = -1.0
        self.i3 = np.dot(self.o2,self.W2)
        oHat = self.sigmoid(self.i3)
        #return oHat
        return oHat, self.o2, self.W1, self.W2

    def feedForwardTwoLayer(self,data_m,i):
        self.i2 = np.dot(data_m[i, :],self.W1)
        oHat = self.sigmoid(self.i2)
        return oHat, self.W1

    def testOutput(self, test_data, hu):
        self.i2 = np.dot(test_data,self.W1)
        self.o2 = self.sigmoid(self.i2)
        if hu == 0:
            return self.o2
        #update the last column as -1, because input from bias=-1
        ncol_o2 = self.o2.shape[0] -1
        self.o2[ncol_o2] = -1.0
        self.i3 = np.dot(self.o2,self.W2)
        oHat = self.sigmoid(self.i3)
        return oHat.ravel()


    def sigmoid(self, x):
        return 1/(1+np.exp(-x))

    def dsigmoid(self, x):
        return np.exp(-x)/((1+np.exp(-x))**2)

class DataAcq(object):

    def get_data(self, path):
        #ml_dataset = parse_c45('example')
        file_name = path.split("/")[-1]
        file_path = '/'.join(path.split("/")[0:-1])+'/.'
        ml_dataset = parse_c45(file_name,file_path)
        dataset_df = np.array(ml_dataset)
        return dataset_df
    
    def read_data_type(self, file_name):
        is_string_numeric = list()
        with open(file_name) as f:
            for line in f:
                if ':' in line:
                    d_typ = line.split(':')[1]
                    d_typ = ''.join(e for e in d_typ if e.isalnum())
                    if d_typ == 'continuous':
                        is_string_numeric.append(True)
                    else:
                        is_string_numeric.append(False)
        return is_string_numeric[1:]



def main_ann(path,cv_fs,hidden_unit,decay_coef,iteration,bag_iter):
    #pdb.set_trace()
    names_f = path.split('/')[-1]
    names_file = path+'/'+names_f+'.names'

    dacq = DataAcq()

    dataset = dacq.get_data(path)
    is_cont = dacq.read_data_type(names_file)

    ncol = dataset.shape[1]
    X = np.delete(dataset,[0,ncol-1],1)
    #converting nominal to continous
    for j in xrange(len(is_cont)):
        if is_cont[j]:
            continue
        nom_values = np.unique(X[:,j])
        for i in xrange(len(nom_values)):
            X[:,j][X[:,j] == nom_values[i] ] = i+1.0

    X = X.astype(float)
    #Normalization of continous variables   
    for j in range(len(is_cont)):
        if is_cont[j]:
            X[:,j] /= np.max(np.abs(X[:,j]),axis=0)
 
    ncolx = X.shape[1]
    #insert bias node
    X = np.insert(X,ncolx,-1.0,axis=1)
    ncolx = X.shape[1]
    Y = dataset[:,ncol-1]
    if Y.dtype.type is np.string_:
        uniq_Y_val = np.unique(Y)
        for i in xrange(len(uniq_Y_val)):
            if uniq_Y_val[i] == "True":
                Y[Y == uniq_Y_val[i]] = 1
            elif uniq_Y_val[i] == "False":
                Y[Y == uniq_Y_val[i]] = 0
    Y = Y.astype(float)
    iteration_count = 0
    old_error = 0.0
    if cv_fs == 0:
        formated_data_for_cv = np.insert(X,ncolx,Y,axis=1)
        #crossValidation return five pair of training and test set
        #In each pair training set has 4 times more datapoints than the test set 
        training_list, test_list = crossValidation(formated_data_for_cv)
        cv_itter = 5
        iteration_count = 0
        ann_acc_arr = []
        ann_prec_arr = []
        ann_rec_arr = []
        ann_auc_arr = []
        #Net = Neural_Network(ncolx,hidden_unit)
        yHatArray = np.empty(X.shape[0]) * np.nan
        globaltestYHat = []
        globalyHatTF = []
        globaltest_output = []

    elif cv_fs == 1:
        cv_itter = 1

    for counter in range(0,cv_itter):
        bag_pred_list_array = []
        for bgc in range(bag_iter):
            if cv_fs == 0:
                num_col = training_list[counter].shape[1]
                X = np.delete(training_list[counter],[num_col-1],1)
                #resampling
                X = bagsample(X)
                Y = training_list[counter][:,num_col-1]
                test_data = np.delete(test_list[counter],[num_col-1],1)
                test_output = test_list[counter][:,num_col-1]
            yHatArray = np.empty(X.shape[0]) * np.nan
            #yHatPrev = np.empty(X.shape[0]) * np.nan
            Net = Neural_Network(ncolx,hidden_unit)
            while True:
                iteration_count += 1
                if iteration > 0:
                    #iteration to check if iteration value is greater than 0
                    #iteration_count += 1
                    if iteration_count >= iteration:
                        print ("Itteration count: {}".format(iteration_count))
                        break
                else:
                    #if itteration is 0 or neg calculate convergence
                    #if change is error is too small, we consider convergence
                    new_error = err(Y, yHatArray)
                      
                    if abs(new_error - old_error) < 0.0000000001:
                        print ("Itteration count: {}".format(iteration_count))
                        break
                    old_error = new_error   
                for i in range(X.shape[0]):
                    if hidden_unit == 0:
                        [yHat, W1] = Net.feedForwardTwoLayer(X, i)
                        newW1 = backTwoLayer(X[i, :], Y[i], yHat, W1, 0.01, decay_coef)#eta=0.01
                        Net.updateWeight(newW1)
                        yHatArray[i] = yHat
                    else:
                        [yHat, hidden, W1, W2] = Net.feedForward(X, i)
                        [newWeightToHidden, newWeightToOutput] = back(X[i, :], hidden, Y[i], yHat, W1, W2, 0.01, decay_coef)
                        Net.updateWeight(newWeightToHidden, newWeightToOutput)
                        yHatArray[i] = yHat

            #pred_listarray
            if cv_fs == 0:
                #calculate yHat output of testData
                testYHat = Net.testOutput(test_data, hidden_unit)
                yHatTF = convertN2TF(testYHat)
            else:
                yHatTF = convertN2TF(yHatArray)                
            bag_pred_list_array.append(yHatTF)

        bag_pred_list_array = np.array(bag_pred_list_array)
        #find the majority vote by using mode
        yHatTF = stats.mode(bag_pred_list_array)[0][0]

        ##BaggingEdits
        if cv_fs == 0:
            #calculate yHat output of testData
            #testYHat = Net.testOutput(test_data, hidden_unit)
            #convert output into True False
            #yHatTF = convertN2TF(testYHat)

            ann_acc = accuracy(yHatTF, test_output)
            ann_prec = precision(yHatTF, test_output)
            ann_rec = recall(yHatTF, test_output)
            ann_acc_arr.append(ann_acc)
            ann_prec_arr.append(ann_prec)
            ann_rec_arr.append(ann_rec)
            #global list for calculating ROC
            globaltestYHat.append(testYHat)
            globalyHatTF.append(yHatTF)
            globaltest_output.append(test_output)
            #last loop of cross validation
            if counter == cv_itter-1:
                print ( "Accuracy: {} {} ".format(np.mean(ann_acc_arr), np.std(ann_acc_arr)))
                print ( "Precision: {} {} ".format(np.mean(ann_prec_arr), np.std(ann_prec_arr)))
                print ( "Recall: {} {} ".format(np.mean(ann_rec_arr), np.std(ann_rec_arr)))

                globaltestYHat = [item for sublist in globaltestYHat for item in sublist]
                globalyHatTF = [item for sublist in globalyHatTF for item in sublist]
                globaltest_output = [item for sublist in globaltest_output for item in sublist]

                auc_val = auc(globaltestYHat, globalyHatTF, globaltest_output)
                print ( "Area Under ROC: {} ".format(auc_val))
        elif cv_fs == 1:
            #yHatTF = convertN2TF(yHatArray)
            ann_acc = accuracy(yHatTF, Y)
            ann_prec = precision(yHatTF, Y)
            ann_rec = recall(yHatTF, Y)
            print ("For Full dataset with no value for Standard Deviations" ) 
            print ( "Accuracy: {} NA".format(ann_acc))
            print ( "Precision: {} NA".format(ann_prec))
            print ( "Recall: {} NA".format(ann_rec))
            auc_val = auc(yHatArray, yHatTF, Y)
            print ( "Area Under ROC: {} ".format(auc_val)) 
        iteration_count = 0
        old_error = 0.0
        yHatArray = np.empty(X.shape[0]) * np.nan
        

#if __name__ == "__main__":
    #main_ann(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    #First Argument is path
    #main_ann("/Users/Chen/Documents/eecs440_fall2017_karimi_zhao/P2/440data/volcanoes",0,100,0.01,100)
    #main_ann(path,cv_fs,hidden_unit,decay_coef,iteration)
    #main_ann(sys.argv[1],0,0,0.001,0)
#    main_ann(sys.argv[1],int(sys.argv[2]),int(sys.argv[3]),float(sys.argv[4]),int(sys.argv[5]))

