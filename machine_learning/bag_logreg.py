import numpy as np
import pdb
from mldata import *
import operator
import math
import random
from cross_validation import *
from scipy import stats
from operator import add


class DataAcq(object):
    def get_data(self, path):
        file_name = path.split("/")[-1]
        file_path = '/'.join(path.split("/")[0:-1]) + '/.'
        ml_dataset = parse_c45(file_name, file_path)
        dataset_df = np.array(ml_dataset)
        return dataset_df

    def read_data_type(self, file_name):
        is_string_numeric = list()
        with open(file_name) as f:
            for line in f:
                if ':' in line:
                    d_typ = line.split(':')[1]
                    d_typ = ''.join(e for e in d_typ if e.isalnum())
                    if d_typ == 'continuous':
                        is_string_numeric.append(True)
                    else:
                        is_string_numeric.append(False)
        return is_string_numeric[1:]


def sigmoid(sc):
    return 1 / (1 + np.exp(-sc))


def gradient_descent(X, Y, wt, m, constant):
    updated_wt_list = []
    # constant = alpha/m
    samp_num = X.shape[0]
    for j in xrange(samp_num):
        del_wt = log_der(Y, X, wt, constant, j, .00001)
        wt = np.subtract(wt, del_wt)
    return wt


def error_method(X, Y, weight):
    Y_hat = np.dot(X, weight)
    error = sum(np.sqrt((Y - Y_hat) ** 2)) * 1.0 / len(Y)

    # print ("Error Method")
    # print (error)
    return error


def accuracy(prd, act):
    if len(prd) != len(act):
        print("accuracy wrong")
        return
    TP = 0
    TN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 0:
            TN = TN + 1
    return (TP + TN) * 1.0 / len(prd)


def log_lkhd(output, inp, weight):
    liklihood = 0
    feature_len = inp.shape[1]
    data_points = inp.shape[0]
    for j in range(0, data_points):
        for i in range(0, feature_len):
            liklihood += output[j] * ((weight[i] * inp[j][i]) + sigmoid(weight[i] * inp[j][i])) * (-1)


def log_der(output, inp, weight, constant, c, lr):
    feature_len = inp.shape[1]
    data_points = inp.shape[0]
    del_weights = []
    z = np.dot(inp[c], weight)
    Y_hat = sigmoid(z)
    # Loop to get delta for each feature
    for j in range(0, feature_len):
        del_val = (((-1) * (output[c] - Y_hat) * inp[c][j]) + constant * weight[j]) * lr
        del_weights.append(del_val)
    return del_weights


def class_pred(prd):
    a = []
    for i in range(0, len(prd)):
        if prd[i] > 0.5:
            a.append(1)
        elif prd[i] <= 0.5:
            a.append(0)
    return a


def precision(prd, act):
    if len(prd) != len(act):
        print("precision wrong")
        return
    TP = 0
    FP = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 1 and act[i] == 0:
            FP = FP + 1
    if TP + FP == 0:
        return float('nan')
    return TP * 1.0 / (TP + FP)


def recall(prd, act):
    if len(prd) != len(act):
        print("recall wrong")
        return
    TP = 0
    FN = 0
    for i in range(0, len(prd)):
        if prd[i] == 1 and act[i] == 1:
            TP = TP + 1
        elif prd[i] == 0 and act[i] == 1:
            FN = FN + 1
    if TP + FN == 0:
        return float('nan')
    return TP * 1.0 / (TP + FN)


def auc(prd_n, prd_tf, act):
    if len(prd_n) != len(prd_tf) and len(prd_tf) != len(act):
        print("length wrong")
        return
    m = np.zeros(shape=[len(prd_n)],
                 dtype=[('prd_n', float), ('prd_tf', float), ('act', float), ('tpr', float), ('fpr', float)])
    for i in range(0, len(prd_n)):
        m[i][0] = prd_n[i]
        m[i][1] = prd_tf[i]
        m[i][2] = act[i]
    # print (m)
    m[::-1].sort(order='prd_n')
    # print(m)
    TP = 0
    allPos = 0
    FP = 0
    allNeg = 0
    for i in range(0, len(act)):
        if m[i][2] == 1:
            allPos = allPos + 1
        elif m[i][2] == 0:
            allNeg = allNeg + 1

    for i in range(0, len(prd_tf)):
        # Calculating TPR and FPR
        if m[i][2] == 1:
            TP += 1
        else:
            FP += 1
        m[i][3] = TP * 1.0 / allPos
        m[i][4] = FP * 1.0 / allNeg

    aroc = 0
    for i in range(0, len(prd_tf) - 1):
        aroc = aroc + 0.5 * (m[i + 1][3] + m[i][3]) * (m[i + 1][4] - m[i][4])

    return aroc


def main_logreg(data_path, cross_vald, lambda_const,bag_itr):
    names_f = data_path.split('/')[-1]
    names_file = data_path + '/' + names_f + '.names'
    # names_file = data_path+'/'+'.names'

    dacq = DataAcq()
    dataset = dacq.get_data(data_path)
    is_cont = dacq.read_data_type(names_file)

    ncol = dataset.shape[1]
    # X = np.delete(dataset,[0,ncol-1],1)
    # X = dataset[:,1:13]
    X = dataset[:, 1:ncol]
    # converting nominal to continous
    for j in xrange(len(is_cont)):
        if is_cont[j]:
            continue
        nom_values = np.unique(X[:, j])
        for i in xrange(len(nom_values)):
            X[:, j][X[:, j] == nom_values[i]] = i + 1.0

    for i in range(0,X.shape[0]):
        if X[i,-1] == 'True':
            X[i,-1] = 1
        elif X[i,-1] == 'False':
            X[i,-1] = 0

    X = X.astype(float)
    # Normalization of continous variables
    for j in range(len(is_cont)):
        if is_cont[j]:
            X[:, j] /= np.max(np.abs(X[:, j]), axis=0)

    #Y = dataset[:, ncol - 1]
    Y = X[:,-1]
    if Y.dtype.type is np.string_:
        uniq_Y_val = np.unique(Y)
        for i in xrange(len(uniq_Y_val)):
            if uniq_Y_val[i] == "True":
                Y[Y == uniq_Y_val[i]] = 1
            elif uniq_Y_val[i] == "False":
                Y[Y == uniq_Y_val[i]] = 0
    Y = Y.astype(float)

    if cross_vald == 0:
        train_set_list, test_set_list = crossValidation(X)
        # weight_coeff =
        cv_itter = 5
        iteration_count = 0
        lgr_acc_arr = []
        lgr_prec_arr = []
        lgr_rec_arr = []
        lgr_auc_arr = []
        # for calcuating AUC
        yHatArray = np.empty(X.shape[0]) * np.nan
        globaltestYHat = []
        globalyHatTF = []
        globaltest_output = []

    elif cross_vald == 1:
        cv_itter = 1
    else:
        print("Please give the correct parameter for CV.")
        return
        
    for counter in range(0, cv_itter):
        
        #Bagging
        pred_array_list = []
        Y_pred_array = []
        for bgc in range(bag_itr):    
            if cross_vald == 0:
                num_col = train_set_list[counter].shape[1]
                X = np.delete(train_set_list[counter], [num_col - 1], 1)
                Y = train_set_list[counter][:, num_col - 1]
                test_data = np.delete(test_set_list[counter], [num_col - 1], 1)
                test_output = test_set_list[counter][:, num_col - 1]
                
                ncolx = X.shape[1]
                # insert 1 for bias weight
                X = np.insert(X, ncolx, 1.0, axis=1)
                test_data = np.insert(test_data, ncolx, 1.0, axis=1)
                X = X.astype(float)
                test_data = test_data.astype(float)
                Y = Y.astype(float)
                test_output = test_output.astype(float)
            else:
                num_col = X.shape[1]
                X = np.delete(X, [num_col - 1], 1)
                X = X.astype(float)
                Y = Y.astype(float)
                ncolx = X.shape[1]
                # insert 1 for bias weight
                X = np.insert(X, ncolx, 1.0, axis=1)
                
            m = len(Y)
            weight_coeff = np.random.uniform(low=0, high=0, \
                                             size=(X.shape[1],)).tolist()
            while_iter = 0
            old_err = 0
            while while_iter < 1500:
                while_iter += 1
                weight_coeff = gradient_descent(X, Y, weight_coeff, m, lambda_const)
            
                err = error_method(X, Y, weight_coeff)
                old_err = err
         
            if cross_vald == 0:
                Y_pred = np.dot(test_data, weight_coeff)
                for i in xrange(len(Y_pred)):
                    Y_pred[i] = sigmoid(Y_pred[i])
                Y_hat = class_pred(Y_pred)
                pred_array_list.append(Y_hat)
                
                Y_pred_array.append(Y_pred)
                #lgr_acc = accuracy(Y_hat, test_output)
                #lgr_prec = precision(Y_hat, test_output)
                #lgr_rec = recall(Y_hat, test_output)
            
            else:
                Y_pred = np.dot(X, weight_coeff)
                for i in xrange(len(Y_pred)):
                    Y_pred[i] = sigmoid(Y_pred[i])
                Y_hat = class_pred(Y_pred)
                pred_array_list.append(Y_hat)
                Y_pred_array.append(Y_hat)
            #lgr_acc = accuracy(Y_hat, Y)
            #lgr_prec = precision(Y_hat, Y)
            #lgr_rec = recall(Y_hat, Y)
        
        pred_array_list = np.array(pred_array_list)
        Y_pred_array = np.array(Y_pred_array)
        #find the majority vote by using mode
        Y_hat = stats.mode(pred_array_list)[0][0]
        Y_pred = stats.mode(Y_pred_array)[0][0]
            
        if cross_vald == 0:
            lgr_acc = accuracy(Y_hat, test_output)
            lgr_prec = precision(Y_hat, test_output)
            lgr_rec = recall(Y_hat, test_output)

            lgr_acc_arr.append(lgr_acc)
            lgr_prec_arr.append(lgr_prec)
            lgr_rec_arr.append(lgr_rec)
            globaltestYHat.append(Y_pred)
            globalyHatTF.append(Y_hat)
            globaltest_output.append(test_output)

            if counter == cv_itter - 1:
                print ("Accuracy: {} {} ".format(np.mean(lgr_acc_arr), np.std(lgr_acc_arr)))
                print ("Precision: {} {} ".format(np.mean(lgr_prec_arr), np.std(lgr_prec_arr)))
                print ("Recall: {} {} ".format(np.mean(lgr_rec_arr), np.std(lgr_rec_arr)))

                globaltestYHat = [item for sublist in globaltestYHat for item in sublist]
                globalyHatTF = [item for sublist in globalyHatTF for item in sublist]
                globaltest_output = [item for sublist in globaltest_output for item in sublist]

                auc_val = auc(globaltestYHat, globalyHatTF, globaltest_output)
                print ("Area Under ROC: {} ".format(auc_val))

        else:
            
            lgr_acc = accuracy(Y_hat, Y)
            lgr_prec = precision(Y_hat, Y)
            lgr_rec = recall(Y_hat, Y)
            print ("For Full dataset with no value for Standard Deviations")
            print ("Accuracy: {} NA".format(lgr_acc))
            print ("Precision: {} NA".format(lgr_prec))
            print ("Recall: {} NA".format(lgr_rec))
            auc_val = auc(Y_pred, Y_hat, Y)
            print ("Area Under ROC: {} ".format(auc_val))
        old_err = 0.0
        Y_pred = np.empty(X.shape[0]) * np.nan
        Y_hat = np.empty(X.shape[0]) * np.nan


if __name__ == "__main__":
    main_logreg(sys.argv[1], int(sys.argv[2]), float(sys.argv[3]))
