import numpy as np
import sys
import operator
import math
import random

def crossValidation(data_array):
    pos = []
    neg = []
    kfold_train_ar = []
    kfold_test_ar = []
    for i in range(0, data_array.shape[0]):
        if data_array[i][-1] == True:
            pos.append(i)
        else:
            neg.append(i)

    p = math.floor(0.2 * data_array.shape[0])
    temp = float(len(pos)) / data_array.shape[0] * p
    if (temp - int(temp) >= 0.5):
        ppn = math.ceil(float(len(pos)) / data_array.shape[0] * p)
    else:
        ppn = math.floor(float(len(pos)) / data_array.shape[0] * p)

    random.seed(12345)
    random.shuffle(pos)
    random.shuffle(neg)

    newArray = []


    c = 0
    while c < 5:
        cPos = ppn
        cNeg = p - ppn
        if c < 4:
            while cPos >= 1:
                newArray.append(data_array[pos[0]])
                tmp = pos[0]
                pos.remove(tmp)
                cPos -= 1
            while cNeg >= 1:
                newArray.append(data_array[neg[0]])
                tmp = neg[0]
                neg.remove(tmp)
                cNeg -= 1
        elif c == 4:
            while len(pos) > 0:
                newArray.append(data_array[pos[0]])
                tmp = pos[0]
                pos.remove(tmp)
            while len(neg) > 0:
                newArray.append(data_array[neg[0]])
                tmp = neg[0]
                neg.remove(tmp)
        c += 1
    npNewArray = np.array(newArray)
    #print (npNewArray)
    for i in range(0, npNewArray.shape[0], int(p)):
        tempTr = []
        tempTs = []
        if i == 0:
            ite = i
            for j in range(0, int(p)):
                tempTs.append(npNewArray[ite])
                ite += 1
            for k in range(0, (npNewArray.shape[0] - int(p))):
                tempTr.append(npNewArray[k + ite])
        elif i == (npNewArray.shape[0] - int(p)):
            for j in range(0, int(p)):
                tempTs.append(npNewArray[j + i])
            for k in range(0, (npNewArray.shape[0] - int(p))):
                tempTr.append(npNewArray[k])
        elif (i+int(p)) > npNewArray.shape[0]:
            break
        else:
            ite = i
            for j in range(0, int(p)):
                tempTs.append(npNewArray[ite])
                ite += 1
            for k in range(0, i):
                tempTr.append(npNewArray[k])
            for m in range(ite, npNewArray.shape[0]):
                tempTr.append(npNewArray[m])
        
        train = np.array(tempTr)
        test = np.array(tempTs)
        kfold_train_ar.append(train)
        kfold_test_ar.append(test)
    return kfold_train_ar, kfold_test_ar

